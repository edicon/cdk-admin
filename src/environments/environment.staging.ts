export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAsiJXWDZhYd6eU7zaca4iivBQMoDL71fY',
    authDomain: 'mvmdash-pro.firebaseapp.com',
    databaseURL: 'https://mvmdash-pro.firebaseio.com',
    projectId: 'mvmdash-pro',
    storageBucket: '',
    messagingSenderId: '724288087185',
    appId: '1:724288087185:web:2dbb41346cc02cb9'
  },
  multibase: {
    apiKey: 'AIzaSyAsiJXWDZhYd6eU7zaca4iivBQMoDL71fY',
    authDomain: 'mvmdash-pro.firebaseapp.com',
    databaseURL: 'https://mvmdash-pro.firebaseio.com',
    projectId: 'mvmdash-pro',
    storageBucket: '',
    messagingSenderId: '724288087185',
    appId: '1:724288087185:web:2dbb41346cc02cb9'
  },
  // Prod: firebase functions
  functions: {
    url: 'https://us-central1-mvmdash-dev.cloudfunctions.net/',
  },
  youTube: {
    dataApiKey: 'AIzaSyB--E628Eg2ccx_cn33L1NenK0xWqf-tuc'
  },
  app: {
    title: 'mVm',
    subTitle: '.net',
    logo: './assets/logo.mvm.png',
    // Enable Right SideNav
    sidenav: {
      right: true
    },
    toolbar: {
      search: true,
      full: true,
      noti: true,
      i18n: true,
      ngxAuth: true,
    },
  },
  ngxAuthConfig: {
    providers: '["google", "facebook", "twitter"]',
    appearance: 'outline', // standard, fill, legacy
    registrationEnabled: true,
    resetPasswordEnabled: true,
    tabIndex: 0, // 0:SignIn, 1:Register
    guestEnabled: true,
    tosUrl: 'https://google.com/',
    privacyPolicyUrl: 'https://google.com/',
    messageOnAuthSuccess: 'The authentication was successful!',
    messageOnAuthError: 'Oop! Something went wrong! Please retry again!',
    goBackURL: '/',
    // Signin Title
    signinTitle: 'Welcome!!',
    // https://pixabay.com/service/faq/
    backgroundImage: 'https://cdn.pixabay.com/photo/2019/06/28/20/22/bridge-4304946__340.jpg'
  },
  i18nConfig: {
    locale: 'kr',
    locales: ['en', 'de', 'fr', 'kr', 'es'],
    localesInfo: [
      { id: 'en', title: 'English',  flag: 'us' },
      { id: 'kr', title: 'Korea',    flag: 'kr' },
      { id: 'de', title: 'German',   flag: 'de' },
      { id: 'fr', title: 'French',   flag: 'fr' },
      { id: 'es', title: 'Española', flag: 'es' }
    ],
    useServer: true,
    // serverUrl: 'http://localhost:8080',
    serverUrl: 'https://us-central1-mvmdash-dev.cloudfunctions.net/',
    env: 'staging'
  },
  mailApi: '/assets/list.json'
};
export const palete = {
  primary: '#D32F2F',
  accent: '#E65100',
  warm: '#C2185B',
  name: '#D50000',
  secondary: '#D81B60',
  tertiary: '#8E24AA',
  quaternary: '#5E35B1',
  quinary: '#3949AB',
  secondaryLight: '#E91E63',
  tertiaryLight: '#9C27B0',
  quaternaryLight: '#673AB7',
  quinaryLight: '#3F51B5'
};
