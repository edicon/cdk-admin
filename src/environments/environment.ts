// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB1GF-DxEkNzTwM-_bV_jxi5qww2dhacuM',
    authDomain: 'mvmdash-dev.firebaseapp.com',
    databaseURL: 'https://mvmdash-dev.firebaseio.com',
    projectId: 'mvmdash-dev',
    storageBucket: '',
    messagingSenderId: '293894597237',
    appId: '1:293894597237:web:2ddc774f10f5cec4'
  },
  multibase: {
    apiKey: 'AIzaSyB1GF-DxEkNzTwM-_bV_jxi5qww2dhacuM',
    authDomain: 'mvmdash-dev.firebaseapp.com',
    databaseURL: 'https://mvmdash-dev.firebaseio.com',
    projectId: 'mvmdash-dev',
    storageBucket: '',
    messagingSenderId: '293894597237',
    appId: '1:293894597237:web:2ddc774f10f5cec4'
  },
  // firebase functions
  functions: {
    url: 'https://us-central1-mglish-service.cloudfunctions.net/',
  },
  youTube: {
    dataApiKey: 'AIzaSyB--E628Eg2ccx_cn33L1NenK0xWqf-tuc'
  },
  app: {
    title: 'mVm',
    subTitle: '.net',
    logo: './assets/logo.mvm.png',
    // Enable Right SideNav
    sidenav: {
      right: true
    },
    toolbar: {
      search: true,
      full: true,
      noti: true,
      i18n: true,
      ngxAuth: true,
    },
  },
  // Ngx-auth-firebase-UI
  ngxAuthConfig: {
    providers: '["google", "facebook", "twitter"]',
    appearance: 'outline', // standard, fill, legacy
    registrationEnabled: true,
    resetPasswordEnabled: true,
    tabIndex: 0, // 0:SignIn, 1:Register
    guestEnabled: true,
    tosUrl: 'https://google.com/',
    privacyPolicyUrl: 'https://google.com/',
    messageOnAuthSuccess: 'The authentication was successful!',
    messageOnAuthError: 'Oop! Something went wrong! Please retry again!',
    goBackURL: '/',
    // Signin Title
    signinTitle: 'Welcome!!',
    // https://pixabay.com/service/faq/
    backgroundImage: 'https://cdn.pixabay.com/photo/2019/06/28/20/22/bridge-4304946__340.jpg'
  },
  i18nConfig: {
    locale: 'kr',
    locales: ['en', 'de', 'fr', /* 'ko', */ 'kr', 'es'],
    localesInfo: [
      { id: 'en', title: 'English',  flag: 'us' },
      // { id: 'ko', title: 'Korea',    flag: 'ko' },
      { id: 'kr', title: 'Korea',    flag: 'kr' },
      { id: 'de', title: 'German',   flag: 'de' },
      { id: 'fr', title: 'French',   flag: 'fr' },
      { id: 'es', title: 'Española', flag: 'es' }
    ],
    // $ cd functions && $ nodemon i18n-server.js
    useServer: false,
    serverUrl: 'http://localhost:8080',
    // serverUrl: 'https://us-central1-mvmdash-dev.cloudfunctions.net/',
  },
  mailApi: '/assets/list.json'
};
export const palete = {
    primary: '#D32F2F',
    accent: '#E65100',
    warm: '#C2185B',
    name: '#D50000',
    secondary: '#D81B60',
    tertiary: '#8E24AA',
    quaternary: '#5E35B1',
    quinary: '#3949AB',
    secondaryLight: '#E91E63',
    tertiaryLight: '#9C27B0',
    quaternaryLight: '#673AB7',
    quinaryLight: '#3F51B5'
};
