import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  query,
} from '@angular/animations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'app';
  constructor( private translate: TranslateService) {
    this.initLocale();
  }

  // Ref: i18nModule
  initLocale() {
    this.translate.addLangs(environment.i18nConfig.locales);
    this.translate.setDefaultLang('en');

    const browserLang = this.translate.getBrowserLang().replace('ko', 'kr');
    if ( !environment.production ) {
      console.log('browserLang', browserLang);
    }
    const matchLocale = environment.i18nConfig.locales.filter( locale => locale === browserLang ).length > 0;
    this.translate.use( matchLocale ? browserLang : 'en');
  }

  getRouteAnimation(outlet) {
    return outlet.activatedRouteData.animation;
  }
}
