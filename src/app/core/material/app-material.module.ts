import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
// import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  imports: [
    // FormsModule,
    CommonModule,
    FlexLayoutModule,
    // FontAwesomeModule,
    OverlayModule,
    DragDropModule,
    MatButtonModule, MatToolbarModule, MatNativeDateModule,
    MatIconModule, MatSidenavModule, MatListModule, MatCardModule,
    MatInputModule, MatSelectModule, MatCheckboxModule,
    MatFormFieldModule, MatTabsModule, MatDialogModule,
    MatStepperModule, MatButtonToggleModule, MatRadioModule,
    MatTableModule, MatPaginatorModule, MatSortModule,
    MatExpansionModule, MatSnackBarModule, MatDatepickerModule,
    MatMenuModule, MatProgressSpinnerModule, // MatSpinner
    MatBottomSheetModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatChipsModule, MatSliderModule
  ],
  exports: [
    // FormsModule,
    CommonModule,
    FlexLayoutModule,
    // FontAwesomeModule,
    OverlayModule,
    DragDropModule,
    MatButtonModule, MatToolbarModule, MatNativeDateModule,
    MatIconModule, MatSidenavModule, MatListModule, MatCardModule,
    MatInputModule, MatSelectModule, MatCheckboxModule,
    MatFormFieldModule, MatTabsModule, MatDialogModule,
    MatStepperModule, MatButtonToggleModule, MatRadioModule,
    MatTableModule, MatPaginatorModule, MatSortModule,
    MatExpansionModule, MatSnackBarModule, MatDatepickerModule,
    MatMenuModule, MatProgressSpinnerModule, // MatSpinner
    MatBottomSheetModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatChipsModule, MatSliderModule
  ],
  // declarations: [ MatSpinner ], 개별 Component에서 import
  // entryComponents: [ MatSpinner ]
})
export class AppMaterialModule { }
