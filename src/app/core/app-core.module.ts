import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { AppLayoutModule } from '@core/layout/app-layout.module';
import { AppMaterialModule } from './material/app-material.module';
import { AppFirebaseModule } from './firebase/app-firebase.module';
import { AppLoginModule } from '@app/pages/login/login.module';
// import { AppI18nModule } from '@core/i18n/i18n.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
    declarations: [
    ],

    imports: [
        CommonModule,
        RouterModule,
        PerfectScrollbarModule,
        // App Modules
        // AppI18nModule
        AppLayoutModule,
        AppMaterialModule,
        AppFirebaseModule,
        AppLoginModule
    ],

    exports: [
        AppLayoutModule,
        AppMaterialModule,
        AppFirebaseModule,
    ],

    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class AppCoreModule { }
