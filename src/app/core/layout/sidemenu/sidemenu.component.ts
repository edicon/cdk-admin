import { Component, OnInit, Input } from '@angular/core';
import { commonMenus, devMenus, prodMenus } from './menu-element';
import { environment } from '@env/environment';

@Component({
  selector: 'cdk-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

    @Input() iconOnly = false;
    public menus = [...commonMenus, ...prodMenus, ...devMenus];

    constructor() {
      if ( environment.production ) {
        this.menus = [...commonMenus, ...prodMenus];
      } else {
        // this.menus = [...devMenus];
        this.menus = [...commonMenus, ...prodMenus, ...devMenus];
      }
    }

    ngOnInit() {
    }

}
