import { Component, OnInit, Input, HostListener, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthProcessService } from 'ngx-auth-firebaseui';
import { Subscription } from 'rxjs';

@Component({
  selector: 'cdk-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit, OnDestroy {
  isOpen = false;
  uid: string;
  mVm = 'Goolish';
  subscription: Subscription;

  @Input() currentUser = null;
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement) {
    if (!targetElement) {
      return;
    }

    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
        this.isOpen = false;
    }
  }

  constructor(
    private router: Router,
    private elementRef: ElementRef,
    private authService: AuthProcessService ) { }

  ngOnInit() {
    this.subscription = this.authService.afa.authState.subscribe( user => {
      if ( user !== null ) {
        this.uid = this.authService.afa.auth.currentUser.uid;
        this.currentUser.currentUserName = (user.displayName !== undefined ? user.displayName : '');
        this.currentUser.photoURL = (user.photoURL !== undefined ? user.photoURL : 'assets/images/avatars/noavatar.png' );
      } else {
      }
    });
  }

  onProfile() {
    // this.router.navigate(['login']);
    this.router.navigate(['/login/ngx-login', 1, 'user']);
  }

  onSettings() {

  }

  onHelp() {

  }

  onLogIn() {
    if ( this.authService.afa.user !== null ) {
      this.authService.afa.auth.signOut().then(() => {
        this.router.navigate(['/login']);
     });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
