import { Component, OnInit, Input } from '@angular/core';
import { Router, Route, ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthProcessService, LinkMenuItem } from 'ngx-auth-firebaseui';
import { ToolbarHelpers } from './toolbar.helpers';
import { environment } from '@env/environment';

@Component({
  selector: 'cdk-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() sidenav;
  @Input() sidebar;
  @Input() drawer;
  @Input() matDrawerShow;

  searchOpen = false;
  toolbarHelpers = ToolbarHelpers;

  env = environment;
  user: firebase.User;
  links: LinkMenuItem[];
  subscription: Subscription;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthProcessService) { }

  ngOnInit() {
    this.subscription = this.authService.afa.user.subscribe( user => {
      if ( !environment.production ) {
        console.log( user );
      }
      if ( user !== null ) {
        this.user = user;
        this.links = [
          {icon: 'home', text: 'Home', callback: this.gotoHome},
          {icon: 'favorite', text: 'Favorite', callback: this.gotoFavority},
          {icon: 'settings', text: 'Setting', callback: this.gotoSetting},
          {icon: 'add', text: 'Add User', callback: this.addUser},
        ];
      } else {
        this.router.navigate(['/login/ngx-login', 0, 'user']);
      }
    });
  }

  gotoHome = () => {
    this.router.navigate(['/main/dashboard'], { queryParams: {home: 'ok'}});
  }

  gotoFavority = () => {
    this.router.navigate(['/main/dashboard'], { queryParams: {favority: 'ok'}});
  }

  gotoSetting = () => {
    this.router.navigate(['/main/dashboard'], { queryParams: {setting: 'ok'}});
  }

  addUser = () => {
    this.router.navigate(['/login/ngx-login', 1, 'user']);
  }

}
