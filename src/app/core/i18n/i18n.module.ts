import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient} from '@angular/common/http';

import { AppMaterialModule } from '../material/app-material.module';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule} from '@ngx-translate/core';

import { I18nComponent } from './i18n.component';
import { environment } from '@env/environment';

export function createTranslateLoader(http: HttpClient) {
  if ( environment.i18nConfig.useServer ) {
    return new TranslateHttpLoader(http, environment.i18nConfig.serverUrl + '/assets/i18n/', '.json');
  }
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    I18nComponent
  ],
  imports: [
    CommonModule,
    // I18nRoutingModule,
    // https://github.com/ngx-translate/core/issues/962
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    AppMaterialModule,
  ],
  exports: [
    TranslateModule, // MUST Be Export
    I18nComponent,
  ]
})
export class AppI18nModule {
}
