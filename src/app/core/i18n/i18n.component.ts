import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment';

export interface LocaleInfo {
  id: string;
  title: string;
  flag: string;
}

@Component({
  selector: 'app-i18n',
  templateUrl: './i18n.component.html',
  styleUrls: ['./i18n.component.scss']
})
export class I18nComponent implements OnInit {

  localesInfo: LocaleInfo[];
  selectedLocale: LocaleInfo;

  constructor(private translateService: TranslateService) {
    if ( !environment.production ) {
      console.log( environment.i18nConfig );
    }

    this.initLocale();

  }

  initLocale() {
    this.translateService.addLangs(environment.i18nConfig.locales);
    this.translateService.setDefaultLang('en');

    const browserLang = this.translateService.getBrowserLang().replace('ko', 'kr');
    if ( !environment.production ) {
      console.log('browserLang', browserLang);
    }
    const matchLocale = environment.i18nConfig.locales.filter( locale => locale === browserLang ).length > 0;
    this.translateService.use( matchLocale ? browserLang : 'en');
  }

  ngOnInit() {

    this.localesInfo = environment.i18nConfig.localesInfo;

    // Set the selected language from default languages
    this.selectedLocale = this.localesInfo.find((locale: any) => {
      if ( !environment.production ) {
        console.log('_translateService.currentLang', this.translateService.currentLang);
        console.log('_translateService.getLangs', this.translateService.getLangs());
        console.log('locale.id === _translateService.currentLang', locale.id === this.translateService.currentLang);
      }
      return locale.id === this.translateService.currentLang;
    });
  }

  /**
   * Set the locale
   * @param lang
   */
  setLocale(lang): void {
    if ( !environment.production ) {
      console.log( lang );
    }
    this.selectedLocale = lang;
    this.translateService.use(lang.id);
  }
}
