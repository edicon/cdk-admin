import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Router } from '@angular/router';
import { AuthProcessService } from 'ngx-auth-firebaseui';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthProcessService,
    private route: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if ( !environment.production ) {
      console.log('canActivate: ', this.auth.afa.auth.currentUser, next, state );
    }

    if ( this.auth.afa.auth.currentUser !== null ) {
      if ( !environment.production ) {
        console.log( this.auth.afa.auth.currentUser );
      }
      return true;
    } else {
      this.route.navigate(['login']);
      return false;
    }
  }
}
