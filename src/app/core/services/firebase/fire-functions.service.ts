import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { take, catchError } from 'rxjs/operators';

import { AngularFireFunctions } from '@angular/fire/functions';

import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class FireFunctionService {

  constructor(
    private httpClient: HttpClient,
    private fns: AngularFireFunctions
  ) {
  }

  getEmailUid( email: string ) {
    const userInfo$ = this.getUserByEmail( email )
    .pipe(
      take(1),
      catchError( err => {
        return of([err]);
      })
    );
    return userInfo$.toPromise();
  }

  getUserByEmail( email: string ): Observable<any> {
    const callable = this.fns.httpsCallable('getUserByEmail');
    return callable({email: email});
  }

  getUserByUid( uid: string ): Observable<any> {
    const callable = this.fns.httpsCallable('getUserByUid');
    return callable({ uid: uid });
  }

  getUserByPhoneNumber( phoneNumber: string ): Observable<any> {
    const callable = this.fns.httpsCallable('getUserByPhoneNumber');
    return callable({phoneNumber: phoneNumber});
  }

  getUserByEmailByHttp( email: string ): Promise<Object> { // Promise<Response> {
    const body = {
      email: email
    };

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    // headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    // headers.append('Access-Control-Allow-Origin', '*');

    return this.httpClient.post(
        environment.functions.url + 'getUserByEmail',
        JSON.stringify(body),
        { headers: headers }
      )
      .toPromise();
  }

  setCustomUserClaims( email: string, userClaims: {} ): Observable<any> {
    const callable = this.fns.httpsCallable('setCustomUserClaims');
    return callable( { email: email, userClaims: userClaims } );
  }

  setCustomUserClaimsByUid( uid: string, userClaims: {} ): Observable<any> {
    const callable = this.fns.httpsCallable('setCustomUserClaimsByUid');
    return callable( { uid: uid, userClaims: userClaims } );
  }

  clearCustomClaims( uid: string, claims: {} ): Observable<any> {
    return this.setCustomUserClaimsByUid( uid, claims);
  }
  clearAllCustomClaims( uid: string,  ): Observable<any> {
    return this.setCustomUserClaimsByUid( uid, {});
  }

  getAllUsers( uid: string, count: number ): Observable<any> {
    const callable = this.fns.httpsCallable('getAllUsers');
    return callable( { uid: uid, count: count } );
  }

  verifyIdToken( idToken: string ): Observable<any> {
    // const idToken = await this.authService.getIdToken();
    const callable = this.fns.httpsCallable('verifyIdToken');
    return callable( {idToken: idToken} );
  }

  // FCM
  fcmSend( request: any ): Observable<any> {
    const callable = this.fns.httpsCallable('fcmSend');
    return callable( request );
  }
  fcmMulticast( request: any ): Observable<any> {
    const callable = this.fns.httpsCallable('fcmMulticast');
    return callable( request );
  }

  sendSMS( request: any ): Observable<any> {
    const callable = this.fns.httpsCallable('sendSMS');
    return callable( request );
  }
}
