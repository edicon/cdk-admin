import { Injectable, NgZone } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class MultibaseService extends AngularFireDatabase {}

export function MultibaseFactory(platformId: Object, zone: NgZone) {
  return new AngularFireDatabase(environment.multibase, environment.multibase.projectId, undefined, platformId, zone);
}

/*
export class MscriptatabaseService extends AngularFireDatabase {}

export function MscriptDatabaseFactory(platformId: Object, zone: NgZone) {
  return new AngularFireDatabase(environment.fireConfig, 'mscript', undefined, platformId, zone);
}
*/
