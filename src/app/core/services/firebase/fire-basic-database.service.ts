import { Injectable } from '@angular/core';
// Firebase
import * as firebase from 'firebase/app';
import { FirebaseAppConfig } from '@angular/fire';
import { AngularFireDatabase, AngularFireList, AngularFireAction, DatabaseSnapshot } from '@angular/fire/database';
import { MultibaseService } from '@app/core/services/firebase/fire-multibase.service';
// RxJs
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BasicDatabaseService {

  total: number;
  countSubject = new BehaviorSubject(0);
  count$: Observable<number>;

  constructor(
    private database: AngularFireDatabase,
    private multibase: MultibaseService
    ) {
      this.count$ = this.countSubject.asObservable();
  }

  // Basic CURD Method
  getValue( url: string ): Observable<any> {
    return this.database.object( url ).valueChanges();
  }
  getSnapshot( url: string ): Observable<AngularFireAction<DatabaseSnapshot<any>>> {
    return this.database.object( url ).snapshotChanges();
  }
  getList( url: string ): Observable<any[]> {
    return this.database.list( url ).valueChanges();
  }
  getListSnapshot( url: string ): Observable<AngularFireAction<DatabaseSnapshot<any>>[]> {
    return this.database.list( url ).snapshotChanges();
  }
  getPushKey(): string {
    return this.database.createPushId();
  }
  pushList( url: string, value: {} ): firebase.database.ThenableReference {
    return this.database.list( url ).push( value );
  }
  set( url: string, value: {} ): Promise<void> {
    return this.database.object( url ).set( value );
  }
  update( url: string, value: {} ): Promise<void> {
    return this.database.object( url ).update( value );
  }
  updates( values: {} ): Promise<void> {
    return this.database.object('/').update( values );
  }
  delete( url: string ) {
    return this.database.object(url).remove();
  }

  /** Function to initialize firebase application and
   * get DB provider for the corresponding application.
   * Usage:  this.fireService.initFirebaseApp(environment.mdic_config, 'mDic');
  **/
  public initFirebaseApp(config: FirebaseAppConfig, firebaseAppName: string) {
  }

  getMultibaseValue( url: string ): Observable<any> {
    return this.multibase.object( url ).valueChanges();
  }
  getMultibaseSnapshot( url: string ): Observable<AngularFireAction<DatabaseSnapshot<any>>> {
    return this.multibase.object( url ).snapshotChanges();
  }
  getMultibaseList( url: string ): Observable<any[]> {
    return this.multibase.list( url ).valueChanges();
  }
  getMultibaseListSnapshot( url: string ): Observable<AngularFireAction<DatabaseSnapshot<any>>[]> {
    return this.multibase.list( url ).snapshotChanges();
  }
  getMultibasePushKey(): string {
    return this.multibase.createPushId();
  }
  pushMultibaseList( url: string, value: {} ): firebase.database.ThenableReference {
    return this.multibase.list( url ).push( value );
  }
  setMultibase( url: string, value: {} ): Promise<void> {
    return this.multibase.object( url ).set( value );
  }
  updateMultibase( url: string, value: {} ): Promise<void> {
    return this.multibase.object( url ).update( value );
  }
  updatesMultibase( values: {} ): Promise<void> {
    return this.multibase.object('/').update( values );
  }
  deleteMultibase( url: string ) {
    return this.multibase.object(url).remove();
  }
}
