import { Injectable } from '@angular/core';

import * as firebase from 'firebase';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { Upload } from '@shared/models/upload-model';


// -https://angularfirebase.com/lessons/angular-file-uploads-to-firebase-storage/
// -https://github.com/codediodeio/angular-firestarter/tree/master/src/app/uploads
@Injectable({
  providedIn: 'root'
})
export class FireStorageService {

  private uploadPath = '/uploads';
  private wordListPath: string;
  private uploadTask: firebase.storage.UploadTask;

  storage: firebase.storage.Storage;
  PUB_DOWN_URL_DEV = 'gs://mglish-service.appspot.com/pub/download/';
  PUB_DOWN_URL_PROD = 'gs://mplayer-27b53.appspot.com/pub/download/';
  PUB_DOWN_URL: string;

  constructor(private db: AngularFireDatabase) {
    this.storage = firebase.storage();
    if ( environment.production ) {
      this.PUB_DOWN_URL = this.PUB_DOWN_URL_PROD;
    } else {
      this.PUB_DOWN_URL = this.PUB_DOWN_URL_DEV;
    }
  }

  // https://code-examples.net/ko-kr/q/2c84785
  getUploads(query= {}): Observable<any[]> {
    // return this.db.list(this.uploadPath, ref => ref.orderByKey());
    return this.db.list(this.uploadPath, ref => ref.orderByKey()).valueChanges();
  }

  // Executes the file uploading to firebase https://firebase.google.com/docs/storage/web/upload-files
  pushUpload(upload: Upload) {
    const storageRef = firebase.storage().ref();
    // this.uploadTask = storageRef.child(`${this.uploadPath}/${upload.file.name}`).put(upload.file);
    // For VocaFile: uid/wordListKey/file.name
    this.uploadTask = storageRef.child(`/voca/uploads/${upload.uid}/${upload.wkey}/${upload.file.name}`).put(upload.file);
    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: any) =>  {
        upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error?) => {
        console.log(error);
      },
      async () => {
        // Firebase 5.2.0:
        // -https://stackoverflow.com/questions/50452457/firebase-storage-dont-return-downloadurl
        upload.url = await this.uploadTask.snapshot.ref.getDownloadURL(); // this.uploadTask.snapshot.downloadURL;
        upload.name = upload.file.name;
        this.saveFileData(upload);
        return null; // Check ? Why null
      }
    );
  }

  pushVideoUpload(upload: Upload) {
    const storageRef = firebase.storage().ref();
    this.uploadTask = storageRef.child(`/video/uploads/${upload.uid}/${upload.wkey}/${upload.file.name}`).put(upload.file);
    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: any) =>  {
        upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error?) => {
        console.log(error);
      },
      async () => {
        // Firebase 5.2.0:
        // -https://stackoverflow.com/questions/50452457/firebase-storage-dont-return-downloadurl
        upload.url = await this.uploadTask.snapshot.ref.getDownloadURL(); // this.uploadTask.snapshot.downloadURL;
        upload.name = upload.file.name;
        this.saveFileData(upload);
        return null; // Check ? Why null
      }
    );
  }

  uploadImage( path: string, file: File) {
    return new Promise((resolve, reject ) =>  {
      const storageRef = firebase.storage().ref();
      // this.uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);
      // For VocaFile: uid/wordListKey/file.name
      this.uploadTask = storageRef.child(`${path}/${file.name}`).put(file);
      this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) =>  {
          const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        },
        (error) => {
          console.log(error);
          reject( error );
        },
        () => {
          // upload.url = this.uploadTask.snapshot.downloadURL;
          // upload.name = upload.file.name;
          // this.saveFileData(upload);
          resolve( );
          // return null;
        }
      );
    });
  }

  download( file: string ) {
    const downUrl = this.PUB_DOWN_URL + file;
    const downRef = this.storage.refFromURL( downUrl );
    return downRef.getDownloadURL();
  }

  downError(error) {
    switch (error.code) {
    case 'storage/object_not_found':
      console.log('File doesn\'t exist');
      break;
    case 'storage/unauthorized':
      console.log('User doesn\'t have permission to access the object');
      break;
    case 'storage/canceled':
      console.log('User canceled the upload');
      break;
    case 'storage/unknown':
      console.log('Unknown error occurred, inspect the server response');
      break;
    }
  }

  // Writes the file details to the realtime db
  private saveFileData(upload: Upload) {
    const saveRef = firebase.database().ref(`${this.uploadPath}/`).push();
    upload.key = saveRef.key;
    upload.timeStamp = firebase.database['ServerValue'].TIMESTAMP;

    saveRef.update( upload );
    // Save Voca File Info
    this.saveVocaFileInfo(upload);
  }

  public saveVocaFileInfo(upload: Upload) {
    this.db.object(`wordList/${upload.wkey}/fileInfo/${upload.type}`).update(upload);
  }

  deleteUpload(upload: Upload) {
    this.deleteFileData(upload.key)
    .then( () => {
      const path = `${this.uploadPath}/${upload.name}`;
      // Not Saved: this.deleteFileStorage(path);
    })
    .catch(error => console.log(error));

    this.deleteVocaFileInfo(upload)
    .then( () => {
      this.deleteVocaFileStorage(upload);
    })
    .catch(error => console.log(error));
  }
  // Writes the file details to the realtime db
  private deleteFileData(key: string) {
    return this.db.list(`${this.uploadPath}/`).remove(key);
  }
  // Firebase files must have unique names in their respective storage dir
  // So the name serves as a unique key
  public deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${name}`).delete();
  }

  // For VocaInfo  Function
  private deleteVocaFileInfo(upload: Upload) {
    return this.db.object(`wordList/${upload.wkey}/fileInfo/${upload.type}`).remove(); // key: fileInfo:audio/image/text
  }
  private deleteVocaFileStorage(upload: Upload) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${upload.uid}/${upload.wkey}/${upload.name}`).delete();
  }

  getDownloadUrl( file: string ) {
    const storageRef = firebase.storage().ref();
    return storageRef.child(file).getDownloadURL();
  }

  // ToDo: Use promise return error message
  handleError( error: any ) {
      switch (error.code) {
        case 'storage/object_not_found':
          // File doesn't exist
          break;
        case 'storage/unauthorized':
          // User doesn't have permission to access the object
          break;

        case 'storage/canceled':
          // User canceled the upload
          break;
        case 'storage/unknown':
          // Unknown error occurred, inspect the server response
          break;
      }
  }
}
