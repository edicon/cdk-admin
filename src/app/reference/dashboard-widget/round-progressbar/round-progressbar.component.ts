import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'cdk-round-progressbar',
  templateUrl: './round-progressbar.component.html',
  styleUrls: ['./round-progressbar.component.scss']
})
export class RoundProgressbarComponent implements OnInit {


    @Input() current;
    @Input() max;
    @Input() background;
    @Input() color;
    @Input() boxcolor;
    @Input() title;



    public radius       =    250;
    public stroke       =    '20' ;
    public semicircle   =    false;
    public rounded      =    true;
    public clockwise    =    false;
    public responsive   =    true;
    public duration     =    '800';
    public animation    =    'easeInOutQuart';

    constructor( private sanitizer: DomSanitizer) { }

    ngOnInit() {
    }
    getOverlayStyle() {
        const isSemi = this.semicircle;
        const transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';
        return {
          'top': isSemi ? 'auto' : '50%',
          'bottom': isSemi ? '5%' : 'auto',
          'left': '50%',
          // 'transform': transform, --> getOverlayTransform();
          // '-moz-transform': transform,
          // '-webkit-transform': transform,
          'font-size': this.radius / 7 + 'px'
        };
    }

    // TODO: Why? No Problem: https://stackoverflow.com/questions/50221969/complex-and-multiple-transform-styles-in-angular-2
    // [style.transform]="getOverlayTransform()"
    // [style.transform]="'translateY(-50%) translateX(-50%)'" // WARNING: sanitizing unsafe style value translateY(-50%) translateX(-50%)
    getOverlayTransform(): SafeStyle {
      const isSemi = this.semicircle;
      const transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';
      return this.sanitizer.bypassSecurityTrustStyle(transform);
    }

}
