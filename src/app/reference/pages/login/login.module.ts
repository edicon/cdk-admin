import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { AppCoreModule } from '@core/app-core.module';

import { NgxAuthFirebaseUIModule, AuthProcessService } from 'ngx-auth-firebaseui';
// import { NgxFireuiLoginComponent } from './ngx-fireui-login/ngx-fireui-login.component';
import { CustomLoginModule} from './custom-login/custom-login.module';

import { environment } from '@env/environment';

const loginRoutes: Routes = [
    // TODO: custom module에 default가 있으면, 먼저 라우팅(Overwriting?)
    // { path: '', component: NgxFireuiLoginComponent },
    // { path: 'ngx-login', component: NgxFireuiLoginComponent },
    { path: 'custom', loadChildren: () =>
      import('./custom-login/custom-login.module').then( m => m.CustomLoginModule)},
];

@NgModule({
  imports: [
    CommonModule,
    AppCoreModule,
    RouterModule.forChild(loginRoutes),
    NgxAuthFirebaseUIModule.forRoot(environment.firebase),
    CustomLoginModule,
  ],
  declarations: [
    // NgxFireuiLoginComponent
  ]
})
export class LoginPageModule { }
