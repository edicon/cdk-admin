import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EditorComponent } from './editor.component';
import { AppCoreModule } from '../../core/app-core.module';
import { Routes, RouterModule } from '@angular/router';
import { QuillModule } from 'ngx-quill';

const routes: Routes = [
    {path: 'editor', component: EditorComponent ,data: { animation: 'editor' }},
  ];
@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        AppCoreModule,
        QuillModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        EditorComponent,
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class EditorModule {
}
