import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AimlDashboardComponent } from './components/aiml-dashboard/aiml-dashboard.component';

const aimlRoutes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  // { path: '', component: AimlDashboardComponent },
  { path: 'dashboard', component: AimlDashboardComponent , data: { aiml: 'dashboard' } },
  { path: 'mobilenet',
    loadChildren: () => import('./modules/mobile-net/mobile-net.module').then( m => m.MobileNetModule)  },
  { path: 'coco-ssd',
    loadChildren: () => import('./modules/coco-ssd/coco-ssd.module').then( m => m.CocoSsDModule)  },
];

@NgModule({
  imports: [RouterModule.forChild(aimlRoutes)],
  exports: [RouterModule]
})
export class AiMlRouterModule { }
