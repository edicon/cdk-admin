import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppCoreModule } from '@app/core/app-core.module';
import { AiMlRouterModule } from './ai-ml-routes.module';
import { AimlDashboardComponent } from './components/aiml-dashboard/aiml-dashboard.component';

import { MobileNetModule } from './modules/mobile-net/mobile-net.module';
import { CocoSsDModule } from './modules/coco-ssd/coco-ssd.module';

@NgModule({
  declarations: [
    AimlDashboardComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppCoreModule,
    AiMlRouterModule,
    MobileNetModule,
    CocoSsDModule
  ]
})
export class AiMlModule { }
