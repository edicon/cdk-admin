import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppCoreModule } from '@app/core/app-core.module';
import { MobileNetRouteModule } from './mobile-net.routes.module';

import { MobileNetDashboardComponent } from './components/mobilenet/mobilenet-dashboard.component';
import { ImageClassifierComponent } from './components/image-classifier/image-classifier.component';
import { VideoClassifierComponent } from './components/video-classifier/video-classifier.component';
import { TransferClassifierComponent } from './components/transfer-classifier/transfer-classifier.component';


@NgModule({
  declarations: [
    MobileNetDashboardComponent,
    ImageClassifierComponent,
    VideoClassifierComponent,
    TransferClassifierComponent,
  ],
  imports: [
    CommonModule,
    AppCoreModule,
    MobileNetRouteModule
  ],
  exports: [
    MobileNetDashboardComponent,
    ImageClassifierComponent,
    VideoClassifierComponent,
    TransferClassifierComponent,
  ]
})
export class MobileNetModule { }
