import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

import * as tf from '@tensorflow/tfjs';
import * as knnClassifier from '@tensorflow-models/knn-classifier';
import * as mobilenet from '@tensorflow-models/mobilenet';

import { environment } from '@env/environment';
import { AngularFireMessaging } from '@angular/fire/messaging';

// pre-trained, transfer, custom classfier
// -https://www.smashingmagazine.com/2019/09/machine-learning-front-end-developers-tensorflowjs/

@Component({
  selector: 'app-transfer-classifier',
  templateUrl: './transfer-classifier.component.html',
  styleUrls: ['./transfer-classifier.component.scss']
})
export class TransferClassifierComponent implements OnInit, AfterViewInit {

  @ViewChild('video') video: ElementRef;

  NUM_CLASSES = 2;
  // Labels for our classes
  classes = ['Left', 'Right'];
  // Webcam Image size. Must be 227.
  IMAGE_SIZE = 227;
  // K value for KNN
  TOPK = 10;

  knn: any;
  mobilenetModule: any;

  infoTexts: string[] = [];

  training: any = true;  // -1 when no class is being trained
  recordSamples = false;
  prediction: any;
  testPrediction = false;

  loading = true;

  constructor() { }

  ngOnInit() {
    this.loadClassifierAndModel();

    this.start();
  }

  async ngAfterViewInit() {
    this.initWebcam();
  }

  async initWebcam() {
    const vid = this.video.nativeElement;

    if (navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true })
        .then((stream) => {
          vid.srcObject = stream;
          vid.width = this.IMAGE_SIZE;
          vid.height = this.IMAGE_SIZE;
        })
        .catch((error) => {
          console.log(`Error: ${error.message}`);
        });
    }

  }

  async loadClassifierAndModel() {
    if ( !environment.production ) {
      console.log('Mobilenet/KCC: Model loading...');
    }
    this.knn = knnClassifier.create();
    this.mobilenetModule = await mobilenet.load();
    if ( !environment.production ) {
      console.log('Mobilenet/KCC: Model loaded.');
    }
    this.setupInfoTexts();

    this.loading = false;
  }

  setupInfoTexts() {
    for (let i = 0; i < this.NUM_CLASSES; i++) {
      this.infoTexts[i] = 'No examples added';
    }
  }

  start() {
    // 1초 단위로 trans-learning prediction
    let i = 0;
    setInterval(async () => {
      if ( !environment.production ) {
        // console.log('timer: ' + i++ );
      }

      const useFunction = true;
      if ( useFunction ) {
        this.runTransferLearning();
      } else {
        this.runTransferLearning2();
      }
      //   await tf.nextFrame();
    }, 1000);
  }

  onLeftClick( event ) {
    this.training = 0;
    this.recordSamples = true;

    this.transferLearning();
    this.getTrainingCount();
  }

  onRightClick( event ) {
    this.training = 1;
    this.recordSamples = true;

    this.transferLearning();
    this.getTrainingCount();
  }

  onTestPrediction() {
    this.testPrediction = !this.testPrediction;
  }

  transferLearning() {
    if ( !environment.production ) {
      console.log('addExample: ' + this.training );
    }

    this.testPrediction = false;

    let logits;
    const image = tf.browser.fromPixels(this.video.nativeElement);
    // 'conv_preds' is the logits activation of MobileNet.
    const infer = () => this.mobilenetModule.infer(image, 'conv_preds');
    logits = infer();
    // Add current image to classifier
    this.knn.addExample(logits, this.training);
  }

  async getTransferPrediction( logits, infer ) {

    const numClasses = this.knn.getNumClasses();

    if (numClasses > 0) {
      // If classes have been added run predict
      logits = infer();
      const res = await this.knn.predictClass(logits, this.TOPK);

      for (let i = 0; i < this.NUM_CLASSES; i++) {
        // The number of examples for each class
        const exampleCount = this.knn.getClassExampleCount();

        // Make the predicted class bold
        // if (res.classIndex === i) {
        //   this.infoTexts[i].style.fontWeight = "bold";
        // } else {
        //   this.infoTexts[i].style.fontWeight = "normal";
        // }

        if (exampleCount[i] > 0) {
          this.infoTexts[i] = ` ${
            exampleCount[i]
          } examples - ${res.confidences[i] * 100}%`;
        }
      }
    }
  }

  getTrainingCount() {
    // The number of examples for each class
    const exampleCount = this.knn.getClassExampleCount();

    for (let i = 0; i < this.NUM_CLASSES; i++) {
      if (exampleCount[i] > 0) {
        this.infoTexts[i] = ` ${exampleCount[i]} examples`;
      }
    }
  }

  async runTransferLearning() {

    if (this.recordSamples) {
      const image = tf.browser.fromPixels(this.video.nativeElement);

      let logits;
      // 'conv_preds' is the logits activation of MobileNet.
      const infer = () => this.mobilenetModule.infer(image, 'conv_preds');
      logits = infer();

      if ( this.testPrediction) {
        this.getTransferPrediction( logits, infer );
      }

      image.dispose();
      if (logits != null) {
        logits.dispose();
      }
    } else {
      if ( this.testPrediction ) {
        alert('먼저 Learning image로 Training 시기기 바랍니다.');
        this.testPrediction = false;
      }
    }
  }

  async runTransferLearning2() {
    if (this.recordSamples) {
      // Get image data from video element
      const image = tf.browser.fromPixels(this.video.nativeElement);

      let logits;
      // 'conv_preds' is the logits activation of MobileNet.
      const infer = () => this.mobilenetModule.infer(image, 'conv_preds');

      // Train class if one of the buttons is held down
      if (this.training !== -1) {
        logits = infer();

        // Add current image to classifier
        if ( !environment.production ) {
          console.log('addExample');
        }
        this.knn.addExample(logits, this.training);
      }

      const numClasses = this.knn.getNumClasses();

      if ( this.testPrediction) {
        this.training = false;
        if (numClasses > 0) {
          // If classes have been added run predict
          logits = infer();
          const res = await this.knn.predictClass(logits, this.TOPK);

          for (let i = 0; i < this.NUM_CLASSES; i++) {
            // The number of examples for each class
            const exampleCount = this.knn.getClassExampleCount();

            // Make the predicted class bold
            // if (res.classIndex === i) {
            //   this.infoTexts[i].style.fontWeight = "bold";
            // } else {
            //   this.infoTexts[i].style.fontWeight = "normal";
            // }

            if (exampleCount[i] > 0) {
              this.infoTexts[i] = ` ${
                exampleCount[i]
              } examples - ${res.confidences[i] * 100}%`;
            }
          }
        }
      }

      if (this.training) {
        // The number of examples for each class
        const exampleCount = this.knn.getClassExampleCount();

        for (let i = 0; i < this.NUM_CLASSES; i++) {
          if (exampleCount[i] > 0) {
            this.infoTexts[i] = ` ${exampleCount[i]} examples`;
          }
        }
      }

      // Dispose image when done
      image.dispose();
      if (logits != null) {
        logits.dispose();
      }
    }
  }

}
