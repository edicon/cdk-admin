import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferClassifierComponent } from './transfer-classifier.component';

describe('TransferClassifierComponent', () => {
  let component: TransferClassifierComponent;
  let fixture: ComponentFixture<TransferClassifierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferClassifierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferClassifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
