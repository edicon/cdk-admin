import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';

import * as mobilenet from '@tensorflow-models/mobilenet';
import * as tf from '@tensorflow/tfjs';
import { Prediction } from '../../models/prediction';

import { environment } from '@env/environment';

@Component({
  selector: 'app-mobilenet-video',
  templateUrl: './video-classifier.component.html',
  styleUrls: ['./video-classifier.component.scss']
})
export class VideoClassifierComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('video') videoElementRef: ElementRef;
  video: HTMLVideoElement;
  localStream: MediaStream;

  model: any;
  loading = true;
  predictions: Prediction[];

  constructor() { }

  ngOnInit() {
     this.initModel();
  }

  ngAfterViewInit() {
    this.initWebCam();
  }

  async initModel() {
    if ( !environment.production ) {
      console.log('Mobilenet(Video): Model loading...');
    }
    this.model = await mobilenet.load();
    if ( !environment.production ) {
      console.log('Mobilenet(Video): Model loaded');
    }
    this.loading = false;
  }

  initWebCam() {
    this.video = this.videoElementRef.nativeElement;

    if (navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true })
        .then((stream) => {
          this.localStream = stream;
          this.video.srcObject = stream;
          this.video.onloadedmetadata = () => {
            this.predictWithMobilenet();
          };
          // this.video.onerror( error => console.log(error ));
        })
        .catch((error) => {
          console.warn('Error', error);
        });
    }
  }

  predictWithMobilenet() {
    setInterval(async () => {
      this.predictions = await this.model.classify(this.video);
      await tf.nextFrame();
    }, 3000);
  }

  videoOff() {
    this.video.pause();
    this.video.src = '';
    this.localStream.getVideoTracks()[0].stop();
    console.log('Video Off');
  }

  ngOnDestroy() {
    console.log('ngOnDestory: video');
    // this.videoOff(); --> to stop, disable setInterval
  }
}
