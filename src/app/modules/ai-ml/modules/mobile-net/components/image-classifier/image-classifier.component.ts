import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import * as mobilenet from '@tensorflow-models/mobilenet';
import { Prediction } from '../../models/prediction';

import { environment } from '@env/environment';

// Create your own image classifier with Angular and Tensorflow.js
// https://blog.angularindepth.com/create-your-own-image-classifier-with-angular-and-tensorflow-js-5b1bc2391424
// Face-Api.js
// -https://github.com/justadudewhohacks/face-api.js

@Component({
  selector: 'app-mobilenet-image',
  templateUrl: './image-classifier.component.html',
  styleUrls: ['./image-classifier.component.scss']
})
export class ImageClassifierComponent implements OnInit {

  imageSrc: string;
  @ViewChild('img') imageEl: ElementRef;

  predictions: Prediction[];

  model: any;
  loading = true;

  constructor() { }

  async ngOnInit() {
    if ( !environment.production ) {
      console.log('Mobilenet(Image): Model loading...');
    }
    this.model = await mobilenet.load();
    if ( !environment. production ) {
      console.log('Mobilenet(Image): Model is loaded!');
    }
    this.loading = false;
  }

  async fileChangeEvent(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (res: any) => {
        this.imageSrc = res.target.result;
        setTimeout(async () => {
          const imgEl = this.imageEl.nativeElement;
          this.predictions = await this.model.classify(imgEl);
        }, 1);

      };
    }
  }
}
