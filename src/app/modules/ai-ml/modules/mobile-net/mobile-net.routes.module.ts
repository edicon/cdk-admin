import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MobileNetDashboardComponent } from './components/mobilenet/mobilenet-dashboard.component';
import { ImageClassifierComponent } from './components/image-classifier/image-classifier.component';
import { VideoClassifierComponent } from './components/video-classifier/video-classifier.component';
import { TransferClassifierComponent } from './components/transfer-classifier/transfer-classifier.component';

const routes: Routes = [
  { path: '', component: ImageClassifierComponent },
  { path: 'dash', component: MobileNetDashboardComponent },
  { path: 'image', component: ImageClassifierComponent, data: { mobilenet: 'image' } },
  { path: 'video', component: VideoClassifierComponent , data: { mobilenet: 'video' } },
  { path: 'transfer', component: TransferClassifierComponent , data: { mobilenet: 'transfer' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MobileNetRouteModule { }
