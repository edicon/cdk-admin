import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CocoSsdVideoComponent } from './coco-ssd-video.component';

describe('CocoSsdVideoComponent', () => {
  let component: CocoSsdVideoComponent;
  let fixture: ComponentFixture<CocoSsdVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CocoSsdVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CocoSsdVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
