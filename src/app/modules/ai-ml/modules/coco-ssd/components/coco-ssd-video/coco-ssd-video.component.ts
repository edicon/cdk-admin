import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
// https://towardsdatascience.com/build-a-realtime-object-detection-web-app-in-30-minutes-7ad0cb2231fb
// The default COCO-SSD is ‘lite_mobilenet_v2’ which is very small in size, under 1MB, and fastest in inference speed.
// For better accurity use ‘mobilenet_v2’, the size of the model increases to 75 MB, which is not suitable for the web-browser experience
import * as cocoSsd from '@tensorflow-models/coco-ssd';
import { DetectedObject } from '@tensorflow-models/coco-ssd';

import { environment } from '@env/environment';

@Component({
  selector: 'app-coco-ssd-video',
  templateUrl: './coco-ssd-video.component.html',
  styleUrls: ['./coco-ssd-video.component.scss']
})
export class CocoSsdVideoComponent implements OnInit, AfterViewInit {

  title = 'TF-ObjectDetection';

  @ViewChild('video') videoElementRef: ElementRef;
  @ViewChild('canvas') canvasElementRef: ElementRef;
  video: HTMLVideoElement;
  // canvas: HTMLCanvasElement;

  width = 600;
  height = 399;
  aspectRatio = 1;

  videoHidden = false;
  loading = true;

  // Version 2.0, Verion 1.x: 300/300
  cocoVersion = 2;
  selectedModel: any = 'lite_mobilenet_v2'; // 'mobilenet_v1';
  model: cocoSsd.ObjectDetection;
  currentDetections: DetectedObject[];

  constructor() { }

  ngOnInit() {
    if ( this.cocoVersion === 1 ) {
      this.width = 300;
      this.height = 300;
    }
  }

  async ngAfterViewInit() {
    await this.initWebCam();
  }

  async initWebCam() {
    if (navigator.mediaDevices.getUserMedia) {
      try {
        const mediaStreamConstraints: MediaStreamConstraints = {
          audio: false,
          video: { facingMode: 'user' },
          // { video: true }
        };
        const stream: MediaStream = await navigator.mediaDevices.getUserMedia(mediaStreamConstraints);
        if ( stream ) {
          this.initVideo( stream );
        }
      } catch ( error ) {
        console.warn('initWebCam: ', error);
      }
    }
  }

  initVideo( stream: MediaStream ) {
    this.video = this.videoElementRef.nativeElement;
    this.video.srcObject = stream;

    this.video.onloadedmetadata = () => {
      if ( !environment.production ) {
        console.log('video.loaded');
      }
      // this.video.play();
      this.videoHidden = true;
      this.predictWithCocoModel();
    };
  }

  async onVideoCanPlay() {
    const videoBoundingRect = this.video.getBoundingClientRect();

    const elementWidth = this.width; // this.video.width;
    const elementHeight = this.height; // this.video.height;
    const intrinsicWidth = this.video.videoWidth;
    const intrinsicHeight = this.video.videoHeight;

    this.aspectRatio = intrinsicWidth / elementWidth;
    if ( !environment.production ) {
      console.log('aspectRatio: ', elementWidth, intrinsicWidth, this.aspectRatio);
    }
  }

  // Models: 'mobilenet_v1' | 'mobilenet_v2' | 'lite_mobilenet_v2'
  async predictWithCocoModel() {
    this.model = await cocoSsd.load({base: this.selectedModel});
    this.loading = false;
    if ( !environment.production ) {
      console.log(`${this.selectedModel}: loaded`);
    }

    this.detectFrame(this.video, this.model);
  }

  // model.detect(
  //   img: tf.Tensor3D | ImageData | HTMLImageElement | HTMLCanvasElement | HTMLVideoElement,
  //   maxDetectionSize: number
  // )
  async detectFrame(video: HTMLVideoElement, model: cocoSsd.ObjectDetection) {
    const currentDetections = await model.detect(video);
    this.currentDetections = currentDetections.map( detection => {
      detection.bbox[0] = detection.bbox[0] / this.aspectRatio;
      detection.bbox[1] = detection.bbox[1] / this.aspectRatio;
      detection.bbox[2] = detection.bbox[2] / this.aspectRatio;
      detection.bbox[3] = detection.bbox[3] / this.aspectRatio;
      return detection;
    });

    this.renderDetections(this.currentDetections);
    requestAnimationFrame(async () => {
      await this.detectFrame(video, model);
    });
  }

  renderDetections( currentDetections) {
    const canvas = <HTMLCanvasElement> this.canvasElementRef.nativeElement;
    const ctx = canvas.getContext('2d');

    canvas.width  = this.width;
    canvas.height = this.height;

    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    // Font options.
    const font = '16px sans-serif';
    ctx.font = font;
    ctx.textBaseline = 'top';
    ctx.drawImage(this.video, 0, 0, this.width, this.height);

    currentDetections.forEach(detection => {
      const x = detection.bbox[0];
      const y = detection.bbox[1];
      const width = detection.bbox[2];
      const height = detection.bbox[3];
      // Draw the bounding box.
      ctx.strokeStyle = '#00FFFF';
      ctx.lineWidth = 2;
      ctx.strokeRect(x, y, width, height);
      // Draw the label background.
      ctx.fillStyle = '#00FFFF';
      const textWidth = ctx.measureText(detection.class).width;
      const textHeight = parseInt(font, 10); // base 10
      ctx.fillRect(x, y, textWidth + 4, textHeight + 4);
    });

    currentDetections.forEach(detection => {
      const x = detection.bbox[0];
      const y = detection.bbox[1];
      // Draw the text last to ensure it's on top.
      ctx.fillStyle = '#000000';
      ctx.fillText(detection.class, x, y);
    });
  }

  async onSelectChanged( event ) {
    if ( !environment.production ) {
      console.log(event, this.selectedModel );
    }

    this.model.dispose();
    this.loading = true;
    this.model =  await  cocoSsd.load( {base: this.selectedModel });
    this.loading = false;

    this.detectFrame(this.video, this.model);
    if ( !environment.production ) {
      console.log(`${this.selectedModel}: loaded`);
    }
  }
}
