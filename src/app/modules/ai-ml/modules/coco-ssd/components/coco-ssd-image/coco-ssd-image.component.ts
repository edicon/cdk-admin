import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
// https://towardsdatascience.com/build-a-realtime-object-detection-web-app-in-30-minutes-7ad0cb2231fb
// The default COCO-SSD is ‘lite_mobilenet_v2’ which is very small in size, under 1MB, and fastest in inference speed.
// For better accurity use ‘mobilenet_v2’, the size of the model increases to 75 MB, which is not suitable for the web-browser experience
import * as cocoSSD from '@tensorflow-models/coco-ssd';
import { environment } from '@env/environment';

@Component({
  selector: 'app-coco-ssd-image',
  templateUrl: './coco-ssd-image.component.html',
  styleUrls: ['./coco-ssd-image.component.scss']
})
export class CocoSsdImageComponent implements OnInit, AfterViewInit {

  @ViewChild('img') imageElementRef: ElementRef;
  // @ViewChild('video', {static: false}) videoElementRef: ElementRef;
  @ViewChild('canvas') canvasElementRef: ElementRef;
  image: HTMLImageElement;
  // canvas: HTMLCanvasElement;

  // Version 2.0, Verion 1.x: 300/300
  cocoVersion = 2;
  width = 600;
  height = 399;

  imageSrc: string;
  videoHidden = false;
  loading = true;

  title = 'CoCo-SSD-Image';
  // selectedModel: any = 'mobilenet_v1';
  selectedModel: any = 'lite_mobilenet_v2';
  model: any;

  constructor() { }

  ngOnInit() {
    if ( this.cocoVersion === 1 ) {
      this.width = 300;
      this.height = 300;
    }
  }

  async ngAfterViewInit() {
    this.loadModel();
    // this.predictWithCocoModel();
  }

  async fileChangeEvent(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (res: any) => {
        this.imageSrc = res.target.result;
        setTimeout(async () => {
          this.image = this.imageElementRef.nativeElement;
          await this.predictWithCocoModel();
        }, 1);

      };
    }
  }

  async loadModel() {
    // Models: 'mobilenet_v1' | 'mobilenet_v2' | 'lite_mobilenet_v2'
    this.loading = true;
    this.model = await cocoSSD.load({base: this.selectedModel});
    this.loading = false;

    if ( !environment.production ) {
      console.log(`${this.selectedModel}: loaded`);
    }
  }

  async predictWithCocoModel() {
    this.detectFrame(this.image, this.model);
  }

  detectFrame = (video, model) => {
    // model.detect(
    //   img: tf.Tensor3D | ImageData | HTMLImageElement | HTMLCanvasElement | HTMLVideoElement,
    //   maxDetectionSize: number
    // )
    model.detect(video).then(predictions => {
      this.renderPredictions(predictions);
      requestAnimationFrame(() => {
        if ( !environment.production ) {
          // tslint:disable-next-line:no-console
          // console.time('predict1');
        }

        this.detectFrame(video, model);

        if ( !environment.production ) {
          // tslint:disable-next-line:no-console
          // console.timeEnd('predict1');
        }
      });
    });
  }

  renderPredictions = predictions => {
    const canvas = <HTMLCanvasElement> this.canvasElementRef.nativeElement;
    const ctx = canvas.getContext('2d');

    canvas.width  = this.width;
    canvas.height = this.height;

    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    // Font options.
    const font = '16px sans-serif';
    ctx.font = font;
    ctx.textBaseline = 'top';
    ctx.drawImage(this.image, 0, 0, this.width, this.height);

    predictions.forEach(prediction => {
      const x = prediction.bbox[0];
      const y = prediction.bbox[1];
      const width = prediction.bbox[2];
      const height = prediction.bbox[3];
      // Draw the bounding box.
      ctx.strokeStyle = '#00FFFF';
      ctx.lineWidth = 2;
      ctx.strokeRect(x, y, width, height);
      // Draw the label background.
      ctx.fillStyle = '#00FFFF';
      const textWidth = ctx.measureText(prediction.class).width;
      const textHeight = parseInt(font, 10); // base 10
      ctx.fillRect(x, y, textWidth + 4, textHeight + 4);
    });

    predictions.forEach(prediction => {
      const x = prediction.bbox[0];
      const y = prediction.bbox[1];
      // Draw the text last to ensure it's on top.
      ctx.fillStyle = '#000000';
      ctx.fillText(prediction.class, x, y);
    });
  }

  async onSelectChanged( event ) {
    if ( !environment.production ) {
      console.log(event, this.selectedModel );
    }

    this.model.dispose();
    await this.loadModel();
    await this.predictWithCocoModel();

    if ( !environment.production ) {
      console.log(`${this.selectedModel}: loaded`);
    }
  }
}
