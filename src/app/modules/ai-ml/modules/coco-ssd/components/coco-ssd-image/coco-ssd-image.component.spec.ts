import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CocoSsdImageComponent } from './coco-ssd-image.component';

describe('CocoSsdComponent', () => {
  let component: CocoSsdImageComponent;
  let fixture: ComponentFixture<CocoSsdImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CocoSsdImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CocoSsdImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
