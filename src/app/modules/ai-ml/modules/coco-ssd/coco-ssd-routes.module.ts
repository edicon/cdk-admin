import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CocoSsdComponent } from './components/coco-ssd/coco-ssd.component';
import { CocoSsdImageComponent } from './components/coco-ssd-image/coco-ssd-image.component';
import { CocoSsdVideoComponent } from './components/coco-ssd-video/coco-ssd-video.component';

const ssdRoutes: Routes = [
  { path: '', component: CocoSsdComponent },
  { path: 'dash', component: CocoSsdComponent , data: { model: 'coco-ssd-dash' } },
  { path: 'image', component: CocoSsdImageComponent , data: { model: 'coco-ssd-image' } },
  { path: 'video', component: CocoSsdVideoComponent , data: { model: 'coco-ssd-video' } },
];

@NgModule({
  imports: [RouterModule.forChild(ssdRoutes)],
  exports: [RouterModule]
})
export class CocoSsdRouteModule { }
