import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppCoreModule } from '@app/core/app-core.module';
import { CocoSsdRouteModule } from './coco-ssd-routes.module';

import { CocoSsdComponent } from './components/coco-ssd/coco-ssd.component';
import { CocoSsdImageComponent } from './components/coco-ssd-image/coco-ssd-image.component';
import { CocoSsdVideoComponent } from './components/coco-ssd-video/coco-ssd-video.component';
import { CocoSsdVideoSvgComponent } from './components/coco-ssd-video-svg/coco-ssd-video-svg.component';


@NgModule({
  declarations: [
    CocoSsdComponent,
    CocoSsdImageComponent,
    CocoSsdVideoComponent,
    CocoSsdVideoSvgComponent
  ],
  imports: [
    CommonModule,
    AppCoreModule,
    CocoSsdRouteModule
  ],
  exports: [
    CocoSsdComponent,
    CocoSsdImageComponent,
    CocoSsdVideoComponent,
    CocoSsdVideoSvgComponent
  ]
})
export class CocoSsDModule { }
