import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiMlComponent } from './aiml-dashboard.component';

describe('AiMlComponent', () => {
  let component: AiMlComponent;
  let fixture: ComponentFixture<AiMlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AiMlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiMlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
