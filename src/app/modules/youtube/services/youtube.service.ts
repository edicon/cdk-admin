import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { getSubtitles } from 'youtube-captions-scraper';

import { YoutubeFunctionService } from '@youtube/services/youtube-functions.service';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class YouTubeService {

  YOUTUBE_VIDEOS_LIST = 'https://www.googleapis.com/youtube/v3/videos?';
  YOUTUBE_CAPTION_LIST = 'https://www.googleapis.com/youtube/v3/captions?';
  YOUTUBE_CAPTION_DOWNLOAD = 'https://www.googleapis.com/youtube/v3/captions';
  YOUTUBE_TIMED_TEXT = 'http://video.google.com/timedtext?';
  YOUTUBE_IFRAME_URL = 'https://www.youtube.com/watch?'; // v=mw5VIEIvuMI

  constructor(
    private httpClient: HttpClient,
    private functionService: YoutubeFunctionService
  ) {
  }

  // part: contentDetails, snippet
  getListInfo( vid: string, part: string ) {
    if ( !environment.production ) {
      console.log( vid );
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type':  'application/json' })
    };
    const query = `${this.YOUTUBE_VIDEOS_LIST}part=${part}&id=${vid}&key=${environment.youTube.dataApiKey}`;
    return this.queryYouTube( query, httpOptions );
  }

  getCaptionInfo( vid: string, part: string ) {
    if ( !environment.production ) {
      console.log( vid );
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type':  'application/json' })
    };
    const query = `${this.YOUTUBE_CAPTION_LIST}part=${part}&videoId=${vid}&key=${environment.youTube.dataApiKey}`;
    return this.queryYouTube( query, httpOptions );
  }

  captionDownload( cid: string ) {
    if ( !environment.production ) {
      console.log( cid );
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type':  'application/json' })
    };
    const query = `${this.YOUTUBE_CAPTION_DOWNLOAD}/${cid}?key=${environment.youTube.dataApiKey}`;
    return this.queryYouTube( query, httpOptions );
  }

  getTimedText( lang: string, vid: string) {
    if ( !environment.production ) {
      console.log( vid );
    }
    const httpOptions = {responseType: 'text'};
    const query = `${this.YOUTUBE_TIMED_TEXT}lang=${lang}&v=${vid}`;
    return this.queryYouTube( query, httpOptions );
  }

  queryYouTube( query: string, options ) {
    if ( !environment.production ) {
      console.log( query );
    }

    const queryInfo$ = this.httpClient.get( query, options ).pipe(
      tap( info => {
        if ( !environment.production ) {
          console.log( info );
        }
      }),
      catchError( err => {
        if ( !environment.production ) {
          console.log( err );
        }
        return of([err]);
      })
    );
    return queryInfo$;

  }

  getYtdlInfo( vid: string ) {
    if ( !environment.production ) {
      console.log( vid );
    }
    const query = `${this.YOUTUBE_IFRAME_URL}v=${vid}`;
    const ytdlInfo$ = this.functionService.youtubeGetInfo({url: query }).pipe(
      tap( info => {
        if ( !environment.production ) {
          console.log( info );
        }
      }),
      catchError( err => {
        if ( !environment.production ) {
          console.log( err );
        }
        return of([err]);
      })
    );
    return ytdlInfo$;
  }

  getScraperCaptions( vid: string, lang: string) {
    return getSubtitles({
      videoID: vid,
      lang: lang
    });
  }

  getScraperCaptionCallable( vid: string, lang: string) {
    const scraperInfo$ = this.functionService.youtubeScraperCaption({ vid: vid, lang: lang }).pipe(
      tap( info => {
        if ( !environment.production ) {
          console.log( info );
        }
      }),
      catchError( err => {
        if ( !environment.production ) {
          console.log( err );
        }
        return of([err]);
      })
    );
    return scraperInfo$;
  }

  // Player
  loadVideo( player: YT.Player, vid: string ) {
    if ( !environment.production ) {
      console.log( vid);
    }
    player.stopVideo();
    player.loadVideoById( vid, 5, 'large');
  }

  playVideo( player: YT.Player ) {
    player.playVideo();
  }

  pauseVideo( player: YT.Player ) {
    player.pauseVideo();
  }
}
