import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { AngularFireFunctions } from '@angular/fire/functions';

import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class YoutubeFunctionService {

  constructor(
    private httpClient: HttpClient,
    private fns: AngularFireFunctions
  ) {
  }

  youtubeGetInfo( request: any ): Observable<any> {
    const callable = this.fns.httpsCallable('youtubeGetInfo');
    return callable( request );
  }

  youtubeScraperCaption( request: any ): Observable<any> {
    const callable = this.fns.httpsCallable('youtubeScraperCaption');
    return callable( request );
  }
}
