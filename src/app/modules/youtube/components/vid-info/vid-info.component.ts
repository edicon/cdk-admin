import { Component, OnInit, OnDestroy, AfterViewInit, Inject, ViewChild } from '@angular/core';

import { Observable, of } from 'rxjs';

import { YouTubeService } from '@youtube/services/youtube.service';

import { environment } from '@env/environment';

@Component({
  selector: 'app-vid-info',
  templateUrl: './vid-info.component.html',
  styleUrls: ['./vid-info.component.scss']
})
export class VidInfoComponent implements OnInit, OnDestroy {

  uid: string;
  // vid = 'zzfCVBSsvqA';
  vid = 'EkHTsc9PU2A';
  player: YT.Player;
  ytEvent: any;
  playerVars = {
    cc_lang_pref: 'en'
  };

  // YouTube API
  videoInfoDetails$: Observable<any>;
  videoInfoSnippet$: Observable<any>;
  // vid: PrAe07KluZY
  captionInfoSnippet$: Observable<any>;
  captionDownload$: Observable<any>;
  timedText$: Observable<any>;

  ytdlInfo$: Observable<any>;
  scraperCaptions: any[];
  scraperCaptions$: Observable<any>;

  production = environment.production;

  constructor(
    private youtubeService: YouTubeService,
  ) {
    if ( !environment.production ) {
    }
  }

  ngOnInit() {
  }

  loadVideo() {
    console.log( this.vid);
    this.player.stopVideo();
    this.player.loadVideoById( this.vid, 5, 'large');
  }

  playVideo() {
    this.player.playVideo();
  }

  pauseVideo() {
    this.player.pauseVideo();
  }

  savePlayer(player) {
    this.player = player;
    console.log('player instance', player);
  }

  onStateChange(event) {
    console.log('player state', event.data);
    this.ytEvent = event.data;
  }

  getVideoInfo() {
    this.videoInfoDetails$ = this.youtubeService.getListInfo( this.vid, 'contentDetails');
    this.videoInfoSnippet$ = this.youtubeService.getListInfo( this.vid, 'snippet');
  }

  getCaptionInfo() {
    this.captionInfoSnippet$ = this.youtubeService.getCaptionInfo( this.vid, 'snippet');
  }

  captionDownload() {
    const captionInfoSnippet$ = this.youtubeService.getCaptionInfo( this.vid, 'snippet');
    captionInfoSnippet$.subscribe(( info: any) => {
      if ( !environment.production ) {
        console.log( info );
      }
      const cid = info.items[0].id;
      this.captionDownload$ = this.youtubeService.captionDownload( cid );
    });
  }

  // return xml NOT json: use httpOptions = {responseType: 'text'};
  // https://stackoverflow.com/questions/23665343/get-closed-caption-cc-for-youtube-video
  async getTimedText() {
    this.timedText$ = this.youtubeService.getTimedText( 'en', this.vid );
  }

  getYtdlInfo() {
    this.ytdlInfo$ = this.youtubeService.getYtdlInfo( this.vid );
  }

  async runCaptionScraper() {
    // this.vid = 'zzfCVBSsvqA';
    // CORS ERROR
    // this.scraperCaptions = await this.youtubeService.getScraperCaptions( this.vid, 'en' );
    // TODO: Debug
    this.scraperCaptions$ = this.youtubeService.getScraperCaptionCallable( this.vid, 'en' );
  }

  ngOnDestroy() {
  }
}
