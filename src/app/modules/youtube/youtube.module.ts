import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCoreModule } from '@app/core/app-core.module';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { YoutubeRouterModule } from './youtube.routes';
import { YoutubeFunctionService } from './services/youtube-functions.service';
import { YouTubeService } from './services/youtube.service';
import { VidInfoComponent } from './components/vid-info/vid-info.component';

@NgModule({
  imports: [
    FormsModule,
    AppCoreModule,
    NgxYoutubePlayerModule.forRoot(),
    YoutubeRouterModule
  ],
  exports: [
    FormsModule,
    VidInfoComponent,
  ],
  declarations: [
    VidInfoComponent
  ],
  providers: [YouTubeService, YoutubeFunctionService],
  entryComponents: [ ]
})
export class YoutubeModule { }
