import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VidInfoComponent } from './components/vid-info/vid-info.component';

const youtubeRoutes: Routes = [
  // { path: '', component: VidInfoComponent },
  { path: '', redirectTo: 'vidinfo', pathMatch: 'full'},
  { path: 'vidinfo', component: VidInfoComponent , data: { animation: 'contact' } },
];

@NgModule({
  imports: [
    RouterModule.forChild(youtubeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class YoutubeRouterModule {}
