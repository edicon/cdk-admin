import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, PLATFORM_ID, NgZone } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
// import { NgEventOptionsModule } from 'ng-event-options';

import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule} from '@ngx-translate/core';

import { AngularFireFunctionsModule, FunctionsRegionToken } from '@angular/fire/functions';
import { MultibaseService, MultibaseFactory } from '@app/core/services/firebase/fire-multibase.service';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { getKoreanPaginatorIntl } from '@shared/locale/korean-paginator-intl';

import { AppCoreModule } from './core/app-core.module';
import { HomeLazyLoadModule } from './home/home-lazy-load.module';

import { AppComponent } from './app.component';
import { AppI18nModule } from '@core/i18n/i18n.module';
import { environment } from '@env/environment';
import { AppSharedModule } from './shared/app-shared.module';

export function createTranslateLoader(http: HttpClient) {
  if ( environment.i18nConfig.useServer ) {
    return new TranslateHttpLoader(http, environment.i18nConfig.serverUrl + '/assets/i18n/', '.json');
  }
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');

}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    // NgEventOptionsModule,
    // App Module
    AppCoreModule,
    AppSharedModule,
    HomeLazyLoadModule,
    // AppI18nModule,
  ],
  exports: [
    TranslateModule
  ],
  providers: [
    { provide: FunctionsRegionToken, useValue: 'us-central1' },
    { provide: MultibaseService, deps: [PLATFORM_ID, NgZone], useFactory: MultibaseFactory },
    { provide: MatPaginatorIntl, useValue: getKoreanPaginatorIntl() }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
