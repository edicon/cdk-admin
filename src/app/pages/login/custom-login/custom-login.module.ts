import { NgModule } from '@angular/core';
import { CustomLoginComponent } from './custom-login.component';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
    // {path: '', component: CustomLoginComponent}, // default routing이 됨
    {path: 'custom', component: CustomLoginComponent},
  ];
@NgModule({
    imports: [
        MatCardModule,
        CommonModule,
        FlexLayoutModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatToolbarModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
      CustomLoginComponent,
    ],
    exports: [
        RouterModule,
        CustomLoginComponent
    ],
    providers: [
    ]
})
export class CustomLoginModule {
}
