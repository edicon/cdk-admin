import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxFireuiLoginComponent } from './ngx-fireui-login.component';

describe('NgxFireuiLoginComponent', () => {
  let component: NgxFireuiLoginComponent;
  let fixture: ComponentFixture<NgxFireuiLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxFireuiLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxFireuiLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
