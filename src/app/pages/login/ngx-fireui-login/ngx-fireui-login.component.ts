import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, Route, ActivatedRoute, ParamMap } from '@angular/router';

import { AuthProcessService, LinkMenuItem } from 'ngx-auth-firebaseui';
import { Subscription } from 'rxjs';
import { environment } from '@env/environment';

@Component({
  selector: 'app-ngx-fireui-login',
  // templateUrl: './test-login.component.html',
  templateUrl: './ngx-fireui-login.component.html',
  styleUrls: ['./ngx-fireui-login.component.scss']
})
export class NgxFireuiLoginComponent implements OnInit, OnDestroy {

  user: firebase.User;
  links: LinkMenuItem[];
  subscription: Subscription;
  routerSubscription: Subscription;

  env = environment;
  authConfig = this.env.ngxAuthConfig;
  signinTitle = this.authConfig.signinTitle;
  tabIndex = this.authConfig.tabIndex;
  profile: string;
  backgroundImage = this.authConfig.backgroundImage;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthProcessService
  ) { }

  // Attribute property binding for background-image url in Angular
  // -https://stackoverflow.com/questions/37577343/attribute-property-binding-for-background-image-url-in-angular-2
  ngOnInit() {
    if ( !this.backgroundImage ) {
      this.backgroundImage = 'https://cdn.pixabay.com/photo/2019/06/28/20/22/bridge-4304946__340.jpg';
    }
    this.routerSubscription = this.route.params.subscribe(params => {
      if ( !environment.production ) {
        console.log( this.authConfig, params );
      }
      const tabIndex = +params['tabIndex']; // (+) converts string 'id' to a number
      if ( tabIndex ) {
        this.tabIndex = tabIndex;
      }
      this.profile = params['profile'];
    });
    if ( !environment.production ) {
      this.links = [
        {icon: 'home', text: 'Home', callback: this.gotoHome},
        {icon: 'favorite', text: 'Favorite', callback: this.gotoFavority},
        {icon: 'add', text: 'Add User', callback: this.addUser},
      ];
    }
    this.subscription = this.authService.afa.user.subscribe( user => {
      if ( !environment.production ) {
        console.log( user );
      }
      this.user = user;
      if ( user !== null ) {
        if ( this.profile !== 'user' ) {
          this.gotoHome();
        }
      }
    });
  }

  gotoHome = () => {
    this.router.navigate(['/main/dashboard'], { queryParams: {login: 'ok'}});
  }

  gotoFavority = () => {
    this.router.navigate(['/main/dashboard'], { queryParams: {login: 'ok'}});
  }

  addUser = () => {
    this.router.navigate(['/login/ngx-login', 1, 'user']);
    this.signOut();
  }

  onLoginSuccess( event ) {
    if ( !environment.production ) {
      console.log('onSuccess: ', event);
    }
    this.router.navigate(['/main/dashboard'], { queryParams: {login: 'ok'}});
  }

  onLoginError() {
    if ( !environment.production ) {
      console.log('onError');
    }
  }

  signOut() {
    this.authService.afa.auth.signOut();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.routerSubscription.unsubscribe();
  }
}
