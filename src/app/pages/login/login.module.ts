import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { AppMaterialModule } from '@app/core/material/app-material.module';
import { NgxAuthFirebaseUIModule, AuthProcessService } from 'ngx-auth-firebaseui';
import { NgxFireuiLoginComponent } from './ngx-fireui-login/ngx-fireui-login.component';
import { CustomLoginModule} from './custom-login/custom-login.module';
import { AppI18nModule } from '@core/i18n/i18n.module';

import { environment } from '@env/environment';

const loginRoutes: Routes = [
    // TODO: custom module에 default가 있으면, 먼저 라우팅(Overwriting?)
    { path: '', component: NgxFireuiLoginComponent },
    { path: 'ngx-login/:tabIndex/:profile', component: NgxFireuiLoginComponent },
    { path: 'custom-login',
        loadChildren: () => import('./custom-login/custom-login.module').then( m => m.CustomLoginModule)},
];

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    AppI18nModule,
    RouterModule.forChild(loginRoutes),
    NgxAuthFirebaseUIModule.forRoot(environment.firebase),
    CustomLoginModule,
  ],
  declarations: [
    NgxFireuiLoginComponent
  ]
})
export class AppLoginModule { }
