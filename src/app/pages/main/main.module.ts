import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// import { MatToolbarModule } from '@angular/material/toolbar';
// import { MatButtonModule } from '@angular/material/button';
// import { MatIconModule } from '@angular/material/icon';
// import { MatSidenavModule } from '@angular/material/sidenav';
// import { MatTabsModule } from '@angular/material';

import { appRoutes } from './main-lazyloader.routes';
import { AppCoreModule } from '@core/app-core.module';
import { DashboardCrmModule } from '../../reference/dashboard-crm/dashboard-crm.module';

import { MainComponent } from './main.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(appRoutes),
        AppCoreModule,
        PerfectScrollbarModule,
        DashboardCrmModule,
        // MatToolbarModule,
        // MatButtonModule,
        // MatIconModule,
        // MatTabsModule,
        // MatSidenavModule,
    ],
    declarations: [MainComponent],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class MainModule { }
