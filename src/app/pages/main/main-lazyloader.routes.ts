import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { DashboardCrmComponent } from '../../reference/dashboard-crm/dashboard-crm.component';

export const appRoutes: Routes = [{
    path: '', component: MainComponent, children: [
        { path: '', redirectTo: 'youtube', pathMatch: 'full'},
        { path: 'dashboard', component: DashboardCrmComponent },
        { path: 'youtube',
          loadChildren: () => import('../../modules/youtube/youtube.module').then( m => m.YoutubeModule) },
        { path: 'aiml',
          loadChildren: () => import('../../modules/ai-ml/ai-ml.module').then( m => m.AiMlModule) },
        { path: 'material-widgets',
          loadChildren: () => import('../../reference/material-widgets/material-widgets.module').then( m => m.MaterialWidgetsModule) },
        { path: 'tables',
          loadChildren: () => import('../../reference/tables/tables.module').then(m => m.TablesModule) },
        { path: 'maps',
          loadChildren: () => import('../../reference/maps/maps.module').then(m => m.MapsModule) },
        { path: 'charts',
          loadChildren: () => import('../../reference/charts/charts.module').then(m => m.ChartsModule) },
        { path: 'pages',
          loadChildren: () => import('../../reference/pages/pages.module').then(m => m.PagesModule) },
        { path: 'forms',
          loadChildren: () => import('../../reference/forms/forms.module').then(m => m.FormModule) },
        { path: 'guarded-routes',
          loadChildren: () => import('../../reference/guarded-routes/guarded-routes.module').then(m => m.GuardedRoutesModule) },
        { path: 'scrumboard',
          loadChildren: () => import('../../reference/scrumboard/scrumboard.module').then(m => m.ScrumboardModule) },
        // { path: 'chats', loadChildren: '../chats/chat.module#ChatsModule' }, // fix this
        // { path: 'mail', loadChildren: '../mail/mail.module#MailModule' }, // fix this
        // { path: 'editor', loadChildren: '../editor/editor.module#EditorModule' },
    ]
}];
