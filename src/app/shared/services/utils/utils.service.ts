import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private snackBar: MatSnackBar,
  ) { }

  openSnackBar( message: string, btn: string, duration?: number ) {
    this.snackBar.open( message, btn, {
      duration: duration
    });
  }
}
