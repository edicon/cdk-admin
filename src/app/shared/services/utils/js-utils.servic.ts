import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JsUtilsService {

  constructor(
  ) { }

  // https://stackoverflow.com/questions/4215737/convert-array-to-object
  array2json( array: any): {} {
    return Object.assign({}, array );
  }

  // https://stackoverflow.com/questions/1960473/get-all-unique-values-in-a-javascript-array-remove-duplicates
  uniquArray( list: string[]): string[] {
    list = list.filter( this.onlyUnique );
    return list;
  }
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
}
