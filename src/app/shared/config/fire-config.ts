export class Config {
    static BASE_URL        = '/';
    // FCM
    static FCM_TOKENS: string = Config.BASE_URL + 'fcmTokens';
    static FCM_LOGS: string = Config.BASE_URL + 'fcmLogs';
}
