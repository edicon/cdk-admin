export class Upload {
    key: string;
    uid: string;
    wkey: string;
    file: File;
    type: string;
    name: string;
    url: string;
    progress: number;
    timeStamp: Object;
    // createdAt: Date = new Date(); // Use TimeStamp of Server

    constructor( uid: string, wkey: string, file: File, type: string) {
        this.uid = uid;
        this.wkey = wkey;
        this.file = file;
        this.type = type;
    }
}
