import { NgModule } from '@angular/core';

import { SpinnerOverlayWrapperComponent } from '@app/shared/modules/spinners/spinner-overlay-wrapper/spinner-overlay-wrapper.component';
import { SpinnerModule } from '@app/shared/modules/spinners/spinner/spinner.module';

@NgModule({
  imports: [SpinnerModule],
  declarations: [SpinnerOverlayWrapperComponent],
  exports: [SpinnerOverlayWrapperComponent]
})
export class SpinnerOverlayWrapperModule {}
