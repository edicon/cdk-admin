import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SpinnerOverlayComponent } from '@app/shared/modules/spinners/spinner-overlay/spinner-overlay.component';
import { SpinnerOverlayService } from '@app/shared/modules/spinners/spinner-overlay/spinner-overlay.service';
import { SpinnerModule } from '@app/shared/modules/spinners/spinner/spinner.module';

@NgModule({
  imports: [CommonModule, SpinnerModule],
  declarations: [SpinnerOverlayComponent],
  entryComponents: [SpinnerOverlayComponent],
  providers: [SpinnerOverlayService],
  exports: []
})
export class SpinnerOverlayModule {}
