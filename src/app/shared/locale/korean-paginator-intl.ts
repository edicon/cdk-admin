import { MatPaginatorIntl } from '@angular/material/paginator';

const koreanRangeLabel = (page: number, pageSize: number, length: number) => {
  if (length === 0 || pageSize === 0) { return `0 중 ${length}`; }

  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // If the start index exceeds the list length, do not try and fix the end index to the end.
  const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} 중 ${length}`;
};


export function getKoreanPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = '페이지당 갯수:';
  paginatorIntl.nextPageLabel = '다음';
  paginatorIntl.previousPageLabel = '이전';
  paginatorIntl.getRangeLabel = koreanRangeLabel;

  return paginatorIntl;
}
