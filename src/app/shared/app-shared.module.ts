import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SpinnerOverlayWrapperModule } from '@app/shared/modules/spinners/spinner-overlay-wrapper/spinner-overlay-wrapper.module';
import { SpinnerModule } from '@app/shared/modules/spinners/spinner/spinner.module';

@NgModule({
  imports: [
    CommonModule,
    SpinnerModule,
    SpinnerOverlayWrapperModule,
    TranslateModule,
  ],
  declarations: [],
  exports: [
    SpinnerModule,
    SpinnerOverlayWrapperModule,
    TranslateModule,
  ]
})
export class AppSharedModule {}
