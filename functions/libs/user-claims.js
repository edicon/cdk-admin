const functions = require('firebase-functions')
const firebaseAdmin = require("firebase-admin");

const debugUserClaims = false;
// const adminUid   =functions.config().admin.uid;
// const adminEmail =functions.config().admin.email

// ksw5062@naver.com
exports.getUserByUid = (data, context) => {

  console.log( data );

  const uid = data.uid;
  if(!(typeof uid === 'string')  || uid.length === 0 )
      throw new functions.https.HttpsError('invalid-argument', 'UID is invalid format.')

  return firebaseAdmin.auth().getUser(uid)
  .then((userRecord) => {
    console.log("getUser: OK" );
    // The claims can be accessed on the user record.
    return userRecord; // userRecord.customClaims
  })
  .catch((e) => {
    console.error("getUser: Error", e.code, e.message);
    throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
  });

}

exports.getUserByEmail = (data, context) => {

  console.log( data );

  const email = data.email;
  if(!(typeof email === 'string')  || email.length === 0 || !validateEmail(email))
      throw new functions.https.HttpsError('invalid-argument', 'Email is invalid format.')

  return firebaseAdmin.auth().getUserByEmail(email)
  .then((userRecord) => {
    console.log("getUserByEmail:", userRecord.toJSON());
    return userRecord.toJSON();
  })
  .catch((e) => {
    console.error("getUserByEmail: Error", e.code, e.message);
    throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
  });
}

exports.getUserByPhoneNumber = (data, context) => {

  console.log( data );

  const phoneNumber = data.phoneNumber;
  if(!(typeof phoneNumber === 'string')  || phoneNumber.length === 0 || !validatePhoneNumber(phoneNumber))
      throw new functions.https.HttpsError('invalid-argument', 'PhoneNumber is invalid format.')

  return firebaseAdmin.auth().getUserByPhoneNumber(phoneNumber)
  .then((userRecord) => {
    console.log("getUserByPhoneNumber:", userRecord.toJSON());
    return userRecord.toJSON();
  })
  .catch((e) => {
    console.error("getUserByPhoneNumber: Error", e.code, e.message);
    throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
  });
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

// TODO: 한국만 체크: 82
// valid: +8210xxxxXXXX invalid: +82010xxxxXXXX
function validatePhoneNumber(phone) {
  return /^\+?(82)([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/.test(phone);
}

exports.sendVerificationEmail = ( userEmail, displayName) => {
  firebaseAdmin.auth().generateEmailVerificationLink(userEmail, actionCodeSettings)
  .then((link) => {
    // Construct email verification template, embed the link and send
    // using custom SMTP server.
    return sendCustomVerificationEmail(userEmail, displayName, link);
  })
  .catch((error) => {
    // Some error occurred.
  });
}

exports.setCustomUserClaims = (data, context) => {

  const debug = true;
  console.log( data, context.auth /*, context */ );

  const email = data.email;
  const userClaims = data.userClaims;
  const userClaimEntries = Object.entries(userClaims);

  if(!(typeof email === 'string')  || email.length === 0 )
      throw new functions.https.HttpsError('invalid-argument', 'email is invalid format.')

  if (!checkPermission( context )) {
      throw new functions.https.HttpsError('invalid-argument', 'Auth is invalid permission')
  }

  return firebaseAdmin.auth().getUserByEmail(email)
  .then( async (user) => {
    let currentCustomClaims = user.customClaims;
    if ( currentCustomClaims === undefined ) {
      currentCustomClaims = {};
    }
    userClaimEntries.forEach( entry => {
      if ( currentCustomClaims !== undefined && ( entry[1] !== null && entry[1] !== undefined) ) {
        currentCustomClaims[entry[0]] = entry[1];
      }
    })
    if( debug ) {
      console.log( user, currentCustomClaims );
    }
    if (user.emailVerified) {
      // USE await to AVOID Nested Promise: then(여기서 Return됨)) --> then(Return 뒤 실행)
      try {
        await firebaseAdmin.auth().setCustomUserClaims(user.uid, currentCustomClaims);
        // Update real-time database to notify client to force refresh.
        // Set the refresh time to the current UTC timestamp.
        // This will be captured on the client to force a token refresh.
        const metadataRef = firebaseAdmin.database().ref("metadata/" + user.uid);
        metadataRef.set({refreshTime: new Date().getTime()});

        return { uid: user.uid, success: true }
      } catch( e ) {
        console.error("setAdminClaims: Error", e.code, e.message);
        throw new functions.https.HttpsError(e.code, e.message);
      }
    } else {
        console.error('email is not verified');
        throw new functions.https.HttpsError('internal', 'email is not verified')
    }
  })
  .catch((e) => {
    console.error(e);
    throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
  });

}

exports.setCustomUserClaimsByUid = (data, context) => {

  const debug = false;
  console.log( data, context.auth /*, context */ );

  const uid = data.uid;
  const userClaims = data.userClaims;
  const userClaimEntries = Object.entries(userClaims);

  if(!(typeof uid === 'string')  || uid.length === 0 )
      throw new functions.https.HttpsError('invalid-argument', 'uid is invalid format.')

  if (!checkPermission( context )) {
      throw new functions.https.HttpsError('invalid-argument', 'Auth is invalid permission')
  }

  return firebaseAdmin.auth().getUser(uid)
  .then( async (user) => {
    let currentCustomClaims = user.customClaims;
    if ( currentCustomClaims === undefined ) {
      currentCustomClaims = {};
    }
    if ( debug ) {
      console.log("1.", currentCustomClaims );
      console.log("2.", userClaimEntries );
    }
    userClaimEntries.forEach( entry => { // enrty: [ admin: true ]
      // 기존에 있던값이 null인경우, empty object으로 clear
      if ( currentCustomClaims !== undefined && ( entry[1] !== null && entry[1] !== undefined) ) {
        currentCustomClaims[entry[0]] = entry[1];
      }
    })
    if( debug ) {
      console.log("3.", user, currentCustomClaims );
    }
    // USE await to AVOID Nested Promise: then(여기서 Return됨)) --> then(Return 뒤 실행)
    try {
      await firebaseAdmin.auth().setCustomUserClaims(user.uid, currentCustomClaims);
      // Update real-time database to notify client to force refresh.
      // Set the refresh time to the current UTC timestamp.
      // This will be captured on the client to force a token refresh.
      const metadataRef = firebaseAdmin.database().ref("metadata/" + user.uid);
      metadataRef.set({refreshTime: new Date().getTime()});

      return { uid: user.uid, success: true }
    } catch( e ) {
      console.error("setCustomUserClaimsByUid: Error", e.code, e.message);
      throw new functions.https.HttpsError(e.code, e.message);
    }
  })
  .catch((e) => {
    console.error(e);
    throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
  });
}

exports.verifyIdToken = (data, context) => {

  console.log( data, context.auth );

  const idToken = data.idToken;
  if(!(typeof idToken === 'string')  || idToken.length === 0 )
      throw new functions.https.HttpsError('invalid-argument', 'idToken is invalid format.')

  return firebaseAdmin.auth().verifyIdToken(idToken)
    .then((decodedToken) => {
      console.log("verifyIdToken:", decodedToken);
      if (decodedToken.admin === true) {
        // Allow access to requested admin resource.
        console.log('admin:', decodedToken.uid);
      }
      return decodedToken;
    })
    .catch((e) => {
      console.log("verifyIdToken: Error", e, e.code, e.message);
      throw new functions.https.HttpsError(e.code, e.message);
    });

}

exports.getAllUsers = async(data, context) => {
  console.log( data, context.auth );
  try {
    let totalUsers = 0;
    totalUsers = await listAllUsers();
    return { users: totalUsers };
  } catch( e ) {
    console.log('Error listing users:', e);
    throw new functions.https.HttpsError(e.code, e.message);
  }
}

let users;
const MAX_USERS = 1000;
async function listAllUsers( nextPageToken) {
  const debug = true;
  if( !nextPageToken ) {
    users = 0;
  }
  const listUsersResult = await firebaseAdmin.auth().listUsers(MAX_USERS, nextPageToken);
  if ( debug ) {
    console.log('0', nextPageToken, users, listUsersResult.pageToken);
  }
  if ( listUsersResult.pageToken ) {
    users = users + listUsersResult.users.length;
    return await listAllUsers(listUsersResult.pageToken);
  } else {
    firebaseAdmin.database().ref('/authClaims').update({totalUsers: users});
    return users;
  }
}

function checkPermission( context ) {
  if ((functions.config().deploy.type === "dev" && (
      // context.auth.uid !== adminUid) ||
      // !context.auth.token.admin ||
      !context.auth.token.email ||
      // !context.auth.token.email.endsWith(adminEmail) ||
      !context.auth.token.email_verified))) {
    // return false
    // console.log(context.auth, adminUid, adminEmail );m
    return true;
  } else {
    return true
  }
}

async function grantModeratorRole(email) {
  const user = await firebaseAdmin.auth().getUserByEmail(email); // 1
  if (user.customClaims && user.customClaims.moderator === true) {
      return;
  }
  return firebaseAdmin.auth().setCustomUserClaims(user.uid, {
      moderator: true
  });
}
