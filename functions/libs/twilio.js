const functions = require('firebase-functions');
const admin = require('firebase-admin');
// admin.initializeApp();
const twilio = require('twilio');

// = JSON.parse(process.env.FIREBASE_CONFIG);
const accountSid = functions.config().twilio.sid;
const authToken  = functions.config().twilio.token;
const twilioNumber = functions.config().twilio.phone;
// const twilioNumber = '+12563054869' // your twilio phone number

const client = new twilio(accountSid, authToken);

// Debug
// console.log(process.env.FIREBASE_CONFIG);
// console.log( twilioNumber);

// remove async
exports.sendSMS = ( data, context) => {

  console.log( data );

  const uid = data.uid;
  const phoneNumber = data.phoneNumber;
  const message = data.body;

  // console.log( uid, mtube );

  if(!(typeof phoneNumber === 'string')  || phoneNumber.length === 0 || !validE164(phoneNumber)) {
      console.error('invalid-argument', `Phone Number(${phoneNumber}) is invalid format.`);
      throw new functions.https.HttpsError('invalid-argument', `Phone Number(${phoneNumber}) is invalid format.`)
  }

  const textMessage = {
    body: message,
    to: phoneNumber,  // Text to this number
    from: twilioNumber // From a valid Twilio number
  }

  return new Promise((resolve, reject ) => {
    client.messages.create(textMessage)
      .then( message => resolve( { // message에 nested reference가 있으면 firebase callabe function에서 에러
        success: true,
        payload: { sid: message.sid }
      }))
      .catch( error => reject( error ));
  });
}

/// Validate E164 format
function validE164(num) {
  return /^\+?[1-9]\d{1,14}$/.test(num)
}
