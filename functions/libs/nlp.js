// Fast and production-ready question answering w/ DistilBERT in Node.js
// -https://github.com/huggingface/node-question-answering#remote-model)
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const QAClient = require("question-answering").QAClient;
const debugLocal = false;

const text = `
  Super Bowl 50 was an American football game to determine the champion of the National Football League (NFL) for the 2015 season.
  The American Football Conference (AFC) champion Denver Broncos defeated the National Football Conference (NFC) champion Carolina Panthers 24–10 to earn their third Super Bowl title. The game was played on February 7, 2016, at Levi's Stadium in the San Francisco Bay Area at Santa Clara, California.
  As this was the 50th Super Bowl, the league emphasized the "golden anniversary" with various gold-themed initiatives, as well as temporarily suspending the tradition of naming each Super Bowl game with Roman numerals (under which the game would have been known as "Super Bowl L"), so that the logo could prominently feature the Arabic numerals 50.
`;

const question = "Who won the Super Bowl?";
// const question = "what is it?";
// const question = "when is it finished?";
// const question = "what's your name?";
// const question = "what do you do today?";

(async () => {
  if( debugLocal ) {
    const qaClient = await QAClient.fromOptions();
    const answer = await qaClient.predict(question, text);
    console.log(answer); // { text: 'Denver Broncos', score: 0.3 }
  }
})();

// 1. firebase: nlp/question write에 triger, nlp/answer에 write
exports.question = async (change, context) => {
    // onCreate: Only edit data when it is first created.
    // if (change.before.exists()) {
    //   return null;
    // }
    // Exit when the data is deleted.
    if (!change.after.exists()) {
      return null;
    }
    // Grab the current value of what was written to the Realtime Database.
    const original = change.after.val();
    console.log('nlp: Question', original);
    const question = original;

    const qaClient = await QAClient.fromOptions(
      {
        // TODO: remote:
        // model: { path: "http://localhost:8501/v1/models/cased", cased: true, remote: true }
        model: { path: "./assets/distilbert-cased", cased: true },
        vocabPath: "./assets/distilbert-cased/vocab.txt"
      }
    );
    const answer = await qaClient.predict(question, text);
    // You must return a Promise when performing asynchronous tasks inside a Functions such as
    // writing to the Firebase Realtime Database. Setting an "answer" sibling in the Realtime Database returns a Promise.
    return change.after.ref.parent.child('answer').set(answer);
};
