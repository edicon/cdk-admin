const firebaseAdmin = require("firebase-admin");
// const LmsPay = require('./lms-pay');

// exports.cronSearch = async ( context ) => {
//   await clearSearchList( context );
// }
// exports.cronLmsPay = async ( context ) => {
//   await LmsPay.evaluateLmsPay( context );
// }
exports.cronTest = async ( context ) => {
  console.log('Cron Test: started..')
}

async function clearSearchList( context ) {
  const queryList = firebaseAdmin.database().ref('/search/queries');
  const resultList = firebaseAdmin.database().ref('/search/results');
  await queryList.set( null )
    .then( _ => console.log('searchCleanup: queryList '))
    .catch( error => console.log(error));
  await resultList.set( null )
    .then( _ => console.log('searchCleanup: resultList '))
    .catch( error => console.log(error));
  console.log('searchCleanupfinished: ', context );
}
