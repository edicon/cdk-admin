const { google } = require('googleapis');
const fetch = require('node-fetch');

const REMOTE_CONDIF_API = 'https://firebaseremoteconfig.googleapis.com/v1/projects'; // my-project-id/remoteConfig
const SCOPES = 'https://www.googleapis.com/auth/firebase.remoteconfig';

const debugRemoteConfig = true;

exports.getRemoteConfigItem = async ( projectId, serviceAccount, item ) => {
  try {
    const accessToken = await exports.getAccessToken( __dirname + '/../' + serviceAccount );
    const configJson = await exports.getRemoteConfig( projectId, accessToken );
    const itemValue =  configJson.parameters[`${item}`].defaultValue;

    if( debugRemoteConfig ) {
      console.log( accessToken );
      console.log( configJson, itemValue );
    }
    return itemValue;
  } catch( error ) {
    console.log(error);
    return error;
  }
}

exports.getRemoteConfig = async ( projectId, token ) => {

  const headers = new fetch.Headers({
    "Authorization": `Bearer ${token}`,
    "Content-Type": "application/json",
    // "Accept-Encoding": "gzip"
  });

  try {
    const API = `${REMOTE_CONDIF_API}/${projectId}/remoteConfig`;
    return await fetch( API, { method: 'GET', headers: headers}).then( res => res.json() );
  } catch( error ) {
    console.log( error );
    return error;
  }
};

exports.getAccessToken = ( serviceAccount ) => {
  return new Promise(function(resolve, reject) {
    const key = require(serviceAccount);
    const jwtClient = new google.auth.JWT(
      key.client_email,
      null,
      key.private_key,
      SCOPES,
      null
    );
    jwtClient.authorize(function(err, tokens) {
      if (err) {
        reject(err);
        return;
      }
      resolve(tokens.access_token);
    });
  });
};

// For Test Only
exports.getRemoteConfigItem('mvmdash-dev', './mvmdash-dev-firebase-adminsdk-2thox-a4df85f2f2.json', 'youtubeFunction')
