const functions = require('firebase-functions');
const process = require("process");
const fs = require('fs');
const util = require('util');

const textToSpeech = require('@google-cloud/text-to-speech');

var client;

var audioEncode = 'MP3';
var langCode = 'en-US';
var gender = 'NEUTRAL';
var projectId;
var keyFilename;
const saveAudio = true;

function initTTS() {

  const projectId = process.env.GCLOUD_PROJECT;
  const keyFilename = functions.config().gcp.keyfilename;
  if( functions.config().deploy.type === "dev" ) {
    console.log("DEPLOY: DEV ", projectId, keyFilename);
    // console.log(functions.config());
  } else {
    console.log("DEPLOY: PROD ", projectId, keyFilename);
  }

  client = new textToSpeech.TextToSpeechClient(
    // {
    //   projectId: projectId,
    //   keyFilename: keyFilename
    // }
  );
}

exports.setParams = ( param ) => {
  switch ( param.type ) {
    case 'audioEncode':
      audioEncode = param.value;
      break;
    case 'langCode':
      langCode = param.value;
      break;
    case 'gender':
      gender = param.value;
      break;
    default:
      console.log('Invalid Params');
      break;
  }
}

exports.textTTS = async ( text ) => {

  if(!(typeof text === 'string')  || text.length === 0) {
    throw new functions.https.HttpsError('invalid-argument', 'tts is invalid format.');
  }

  initTTS();

  const request = {
    input: {text: text},
    // Select the language and SSML Voice Gender (optional)
    voice: {languageCode: langCode, ssmlGender: gender},
    audioConfig: {audioEncoding: audioEncode},
  };

  const [response] = await client.synthesizeSpeech(request);

  if ( saveAudio ) {
    const writeFile = util.promisify(fs.writeFile);
    await writeFile('text.mp3', response.audioContent, 'binary');
    console.log('Audio content written to file: text.mp3');
  } else {
    return response;
  }
}

exports.ssmlTTS = async ( ssml ) => {

  if(!(typeof ssml === 'string')  || ssml.length === 0) {
    throw new functions.https.HttpsError('invalid-argument', 'ssml is invalid format.');
  }

  initTTS();

  const request = {
    input: {ssml: ssml},
    voice: {languageCode: langCode, ssmlGender: gender},
    audioConfig: {audioEncoding: audioEncode},
  };

  const [response] = await client.synthesizeSpeech(request);

  if ( saveAudio ) {
    const writeFile = util.promisify(fs.writeFile);
    await writeFile('ssml.mp3', response.audioContent, 'binary');
    console.log('Audio content written to file: ssml.mp3');
  } else {
    return response;
  }
}

// For Standalone Test: $ node ./libs/tts.js
async function testTTS()  {
  const text = 'Hello, world!'
  await exports.textTTS( text );

  const ssml = '<speak>Hello there.</speak>';
  await exports.ssmlTTS( ssml );
}

// testTest().catch(console.error);
