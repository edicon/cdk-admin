const admin = require("firebase-admin")
const functions = require('firebase-functions')

const debugFCM = false;

const FCM_TOKENS = functions.config().fcm.tokens;
const FCM_TOKENS2 = functions.config().fcm.tokens;
const FCM_LOGS = functions.config().fcm.logs;
const MAX_FCM_SEND = 100;

exports.testMessage = ( message ) => {
  sendMessage( message ).then((response) => {
    return response;
  })
  .catch((error) => {
    return error;
  });;
}

exports.sendMessage = async ( data, context) => {

  console.log( data );

  const uid = data.uid;
  const message = data.message;
  const notification = data.message.notification;
  const title = notification.title;
  const body = notification.body;
  const mtube = data.mtube;

  // console.log( uid, mtube );

  if(!(typeof uid === 'string')  || uid.length === 0 ) {
      console.error('invalid-argument', `UID${uid} is invalid format.`);
      throw new functions.https.HttpsError('invalid-argument', `UID${uid} is invalid format.`)
  }

  let token;
  if ( mtube ) {
    token = await getMtubeToken( uid );
  } else {
    token = await getToken( uid );
  }
  if(!(typeof token === 'string')  || token.length === 0 ) {
      console.error('invalid-argument', `TOKEN(${token}) is invalid.`);
      throw new functions.https.HttpsError('invalid-argument', `TOKEN(${token}) is invalid.`)
  }

  message['token'] = token;

  return sendMessage( message )
  .then((response) => {
    if ( debugFCM ) console.log("sendMessage: OK", response );
    return response;
  })
  .catch((e) => {
    console.error("sendMessage: Error", e.code, e.message);
    throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
  });
}

exports.sendMulticast = async ( data, context) => {

  console.log( data );
  const debugMulticast = false;

  const totalUids = data.uids; // array
  const message = data.message;
  const notification = data.message.notification;
  const uid = data.uid;
  const key = data.key;
  const mtube = data.mtube;
  if ( debugFCM ) {
    const title = notification.title;
    const body = notification.body;
    const mdata = data.message.data;
  }
  if( totalUids.length === 0 ) {
      console.error('invalid-argument', 'UID(Multicast) is invalid format.');
      throw new functions.https.HttpsError('invalid-argument', 'UID(Multicast) is invalid format.')
  }

  // if ( uids.length > 100 ) {
  let failureCount = 0;
  let successCount = 0;

  const chunkedUids = chunkUids( totalUids );

  for( i = 0; i < chunkedUids.length; i++) {
    let promiseAll;
    const uids = chunkedUids[i];
    if ( mtube ) {
      promiseAll = uids.map( uid => getMtubeToken( uid ));
    } else {
      promiseAll = uids.map( uid => getToken( uid ));
    }

    const tokens = await Promise.all( promiseAll );
    message['tokens'] = tokens;

    try {

      const response = await sendMulticast( message )

      delete message.tokens;
      logFcmSend( uid, key, data );
      logFcmResponse( uid, key, response );
      delete response.responses;

      failureCount += response.failureCount;
      successCount += response.successCount;
      if ( debugFCM  || debugMulticast ) console.log( uids, tokens, response );

      /// return sendMulticast( message )
      // .then((response) => {
      //   if ( debugFCM || debugMulticast  ) console.log("sendMulticast: OK", response );
      //   // TODO: save response.reponses to log, return failureCount/success
      //   delete message.tokens;
      //   logFcmSend( uid, key, data );
      //   logFcmResponse( uid, key, response );
      //   delete response.responses;
      //   return response;
      // })
      // .catch((e) => {
      //   console.error("sendMulticast: Error", e.code, e.message);
      //   throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
      // });

    } catch ( e) {
      console.error("sendMulticast: Error", e.code, e.message);
      throw new functions.https.HttpsError('internal', e.code + ': ' + e.message);
    }
  }

  const results = {};
  results['failureCount'] = failureCount;
  results['successCount'] = successCount;
  return results;
}

function chunkUids( uids ) {
  let i,j,chunks = [], chunk = MAX_FCM_SEND;
  for (i=0,j=uids.length; i<j; i+=chunk) {
    chunks.push(uids.slice(i,i+chunk));
  }
  console.log( chunks );
  return chunks;
}

function sendMessage( message ) {
  return admin.messaging().send(message);
}

function sendMulticast( message ) {
  return admin.messaging().sendMulticast(message);
  // .then((response) => {
  //   if (response.failureCount > 0) {
  //     const failedTokens = [];
  //     response.responses.forEach((resp, idx) => {
  //       if (!resp.success) {
  //         failedTokens.push(registrationTokens[idx]);
  //       }
  //     });
  //     console.log('List of tokens that caused failures: ' + failedTokens);
  //   }
  // });
}

async function getToken( uid ) {
  const snapshot = await admin.database().ref(`${FCM_TOKENS}/${uid}`).once('value');
  const token = snapshot.val();
  return token;
}
async function getMtubeToken( uid ) {
  const snapshot = await admin.database().ref(`${FCM_TOKENS2}/${uid}/fcm_token`).once('value');
  const token = snapshot.val();
  return token;
}

function logFcmSend( uid, key, data ) {
  data['timestamp'] = admin.database.ServerValue.TIMESTAMP;

  const ref = admin.database().ref(`${FCM_LOGS}/${uid}/${key}/send`)
  ref.push(data);
}
function logFcmResponse( uid, key, response ) {
  response['timestamp'] = admin.database.ServerValue.TIMESTAMP;

  const ref = admin.database().ref(`${FCM_LOGS}/${uid}/${key}/response`)
  ref.push(response);
}
