// https://www.npmjs.com/package/ytdl-core
const ytdl = require('ytdl-core');
const getSubtitles = require('youtube-captions-scraper').getSubtitles;

const testURL = 'https://www.youtube.com/watch?v=mw5VIEIvuMI';
const debugYTDL = false;

exports.getInfo = ( data, context ) => {
  const url = data.url;

  if( !ytdl.validateURL(url) ) {
    console.error('invalid-argument', `Video URL(${url}) is invalid format.`);
    throw new functions.https.HttpsError('invalid-argument', `Video URL(${url}) is invalid format.`)
  }

  const vid = ytdl.getURLVideoID(url)
  return new Promise((resolve, reject ) => {
    ytdl.getInfo(vid, ( e, info) => {
      if ( e ) throw new functions.https.HttpsError(e.code, e.message);
      if ( debugYTDL ) console.log( info );
      resolve( info );
    });
  });
}

exports.getScraperCaption = ( data, context ) => {
  const vid = data.vid;
  const lang = data.lang;
  console.log( data );
  if(!(typeof vid === 'string')  || vid.length === 0 ) {
    console.error('invalid-argument', `Video ID(${vid}) is invalid format.`);
    throw new functions.https.HttpsError('invalid-argument', `Video ID(${vid}) is invalid format.`)
  }
  try {
    return getSubtitles({
      videoID: vid,
      lang: lang
    });
  } catch( error ) {
    console.error(error);
    throw new functions.https.HttpsError('invalid-argument', `getSubtitle: Errro(Code:Message): ${error.code}: ${error.message}`)
  }
}
