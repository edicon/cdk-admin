const functions = require('firebase-functions')
const firebaseAdmin = require("firebase-admin");
const LmsPay = require('./lms-pay');
const fcm = require('./fcm');

// ** https://firebase.google.com/docs/functions/get-started
// Firebase projects on the Spark plan can make only outbound requests to Google APIs.
// Requests to third-party APIs fail with an error.
// 즉 유료 account에 대해서만, 외부 API call 허용

// Kakao API request url to retrieve user profile based on access token
const requestMeUrl = 'https://kapi.kakao.com/v1/user/me';
// Seperate Dev and Prod Env.
const devDatabaseUrl = "https://mglish-service.firebaseio.com";
const devServiceAccount = require("./mglish-service-firebase-adminsdk-ttx6i-140d5ac174.json");
const prodDatabaseUrl = "https://mplayer-27b53.firebaseio.com";
const prodServiceAccount = require("./mplayer-27b53-firebase-adminsdk-kk33u-5002b28e42.json");

const devServiceConfig = {
  credential: firebaseAdmin.credential.cert(devServiceAccount),
  databaseURL: devDatabaseUrl
}
const prodServiceConfig = {
  credential: firebaseAdmin.credential.cert(prodServiceAccount),
  databaseURL: prodDatabaseUrl
}

let defaultConig = devServiceConfig;
// const adminUid   =functions.config().admin.uid;
// const adminEmail =functions.config().admin.email;
// if( functions.config().deploy.type === "prod" ) {
//   console.log("DEPLOY: PROD: Admin: ", adminEmail, adminUid)
//   defaultConig = prodServiceConfig;
// } else {
//   console.log("DEPLOY: DEV: Admin: ", adminEmail, adminUid)
//   console.log('Algoria: DEV=PROD Key: ', functions.config().algolia.app_id, functions.config().algolia.api_key)
// }
const defaultApp = firebaseAdmin.initializeApp( defaultConig );

// evaluateLmsPay();
sendMessage();

function sendMessage() {
  var registrationToken = 'fnCPA2tAa28:APA91bGach-Y9koBXppQ7bjS3cQNJ2vGZ1oIYSWeQzhHtbp4l2jbFsfp9FXqwOUy8qbExaFa1e796WAPu4ZULwyfls9rrMMbwdKF28siCuqIUbQqCiNHav4fxLs0wvYmjCy5HL7q6GC1';

  var message = {
    data: {
      score: '850',
      time: '2:45'
    },
    token: registrationToken
  };
  fcm.testMessage( message );
}

async function evaluateLmsPay() {
  await LmsPay.evaluatePay();
  await LmsPay.evaluateFree();
}

// Setting Cache-Control: https://firebase.google.com/docs/hosting/functions
// res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
exports.bigben = ( req, res) => {
  const hours = (new Date().getHours() % 12) + 1; // london is UTC + 1hr;
  res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
  res.set('Vary', 'Accept-Encoding, X-My-Custom-Header');
  res.status(200).send(
    `<!doctype html>
      <head>
        <title>Time</title>
      </head>
      <body>
        ${'BONG '.repeat(hours)}
      </body>
    </html>`
  );
}

