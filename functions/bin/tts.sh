#!/bin/bash -x
# bin folder에서 실행
export GOOGLE_APPLICATION_CREDENTIALS=../mvmdash-dev-678ae8877b5e.json
curl -H "Authorization: Bearer $(gcloud auth application-default print-access-token)" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data "{
    'input':{
      'text':'Android is a mobile operating system developed by Google,
         based on the Linux kernel and designed primarily for
         touchscreen mobile devices such as smartphones and tablets.'
    },
    'voice':{
      'languageCode':'en-gb',
      'name':'en-GB-Standard-A',
      'ssmlGender':'FEMALE'
    },
    'audioConfig':{
      'audioEncoding':'MP3'
    }
  }" "https://texttospeech.googleapis.com/v1/text:synthesize" > synthesize-text.txt

cp  synthesize-text.txt synthesize-output-base64.txt

echo "synthesize-output-base64.txt을 editing 바랍니다."
# {
#   "audioContent": "//NExAASCCIIAAhEAGAAEMW4kAYPnwwIKw/BBTpwTvB+IAxIfghUfW.."
# }
# vi synthesize-output-base64.txt
# 1,3라인 제거, 2라인 "audioContent": " ............ "

base64 synthesize-output-base64.txt --decode > synthesized-audio.mp3
