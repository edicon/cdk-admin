#!/bin/sh -x
FUNC_PWD=`pwd`
echo "Firebase PROD."
# echo $FUNC_PWD

# 관리자 인증정보 설정
# https://firebase.google.com/docs/functions/local-emulator
export GOOGLE_APPLICATION_CREDENTIALS="mvmdash-pro-firebase-adminsdk-80pwb-13027ef41d.json"
firebase use mvmdash-pro

# 환경 구성
# .runfimeconfig: https://firebase.google.com/docs/functions/config-en
firebase functions:config:set deploy.type="prod"
firebase functions:config:set gcp.projectid="mvmdash-pro" gcp.keyfilename="mvmdash-pro-280c3244682f.json"h

# FCM Token Url in firebase
firebase functions:config:set fcm.tokens="fcmTokens"
firebase functions:config:set fcm.logs="fcmLogs"

# Twillo
firebase functions:config:set twilio.sid="ACaeede17f27b7206be6eb77452bb735a6"
firebase functions:config:set twilio.token="feeffa3a6311288cc77fcde8a79409e4"
firebase functions:config:set twilio.phone="+12563054869"

## Save config for local emularor
# https://github.com/firebase/firebase-tools/issues/678
firebase functions:config:get > ./functions/.runtimeconfig.json
firebase functions:config:get

firebase list

echo "*** RUN EXPOER ***"
echo "export GOOGLE_APPLICATION_CREDENTIALS=mvmdash-pro-firebase-adminsdk-80pwb-13027ef41d.json"
