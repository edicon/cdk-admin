#!/bin/sh -x
echo "Firebase DEV."
FUNC_PWD=`pwd`
echo $FUNC_PWD

# 관리자 인증정보 설정
# https://firebase.google.com/docs/functions/local-emulator
# !!! 중요: firebase deploy할 때는, 변수 Reset GOOGLE_APPLICATION_CREDENTIALS
export GOOGLE_APPLICATION_CREDENTIALS="mvmdash-dev-firebase-adminsdk-2thox-a4df85f2f2.json"
firebase use mvmdash-dev

# 환경 구성
# https://firebase.google.com/docs/functions/config-env
firebase functions:config:set deploy.type="dev"
firebase functions:config:set gcp.projectid="mvmdash-dev" gcp.keyfilename="mvmdash-dev-678ae8877b5e.json"

# FCM Token Url in firebase
firebase functions:config:set fcm.tokens="fcmTokens"
firebase functions:config:set fcm.logs="fcmLogs"

# Twillo
firebase functions:config:set twilio.sid="ACaeede17f27b7206be6eb77452bb735a6"
firebase functions:config:set twilio.token="feeffa3a6311288cc77fcde8a79409e4"
firebase functions:config:set twilio.phone="+12563054869"


## Save config for local emularor
# .runfimeconfig: https://firebase.google.com/docs/functions/config-en
firebase functions:config:get > ./functions/.runtimeconfig.json
firebase functions:config:get

firebase list

echo "*** RUN EXPOER ***"
echo "export GOOGLE_APPLICATION_CREDENTIALS=mvmdash-dev-firebase-adminsdk-2thox-a4df85f2f2.json"
