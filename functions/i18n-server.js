var path = require('path');
const express = require('express');
var cors = require('cors');
var app = express();

app.use(cors());

app.use('/assets', express.static(path.resolve(__dirname, 'assets')));
var apiRoutes = express.Router();
app.use('/api', apiRoutes);

const port = 8080;
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
