const path = require('path');
const express = require('express');
const cors = require('cors');
const app = express();

const functions = require('firebase-functions');
const admin = require("firebase-admin");
const process = require("process");
// My Libs
const Cron = require('./libs/cron');
const UserClaims = require('./libs/user-claims');
const FCM = require('./libs/fcm');
const Twilio = require('./libs/twilio');
const YouTube = require('./libs/youtube');
const TTS = require('./libs/tts');
const RemoteConfig = require('./libs/remote-config');
const NLP = require('./libs/nlp');

// Check Env.
if( functions.config().deploy.type === "prod" ) {
  console.log("DEPLOY: PROD: ");
} else {
  console.log("DEPLOY: DEV: ");
  console.log(process.env.GCLOUD_PROJECT);
  console.log(process.env.FIREBASE_CONFIG);
  console.log(functions.config());
}

app.use(cors());

app.use('/assets', express.static(path.resolve(__dirname, 'assets')));
var apiRoutes = express.Router();
app.use('/api', apiRoutes);

exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});

// Init App
const defaultApp = admin.initializeApp();
// CRON: clear, payment
// '5 11 * * *': every day at 11:05 AM UTC
// exports.cronJobs = functions.pubsub.schedule('every 2 minutes').onRun( async (context) => {
exports.cronJobs = functions.pubsub.schedule('every day 00:00').onRun( async (context) => {
  // await Cron.cronSearch( context );
  // await Cron.cronLmsPay( context );
});
exports.cronTest = functions.pubsub.schedule('every 2 minutes').onRun( async (context) => {
  console.log('This will be run every 2 minutes!');
  await Cron.cronTest( context );
});

// Custom Claims
exports.setCustomUserClaims = functions.https.onCall((data, context) => {
  return UserClaims.setCustomUserClaims( data, context);
});

exports.setCustomUserClaimsByUid = functions.https.onCall((data, context) => {
  return UserClaims.setCustomUserClaimsByUid( data, context);
});

exports.verifyIdToken = functions.https.onCall((data, context) => {
  return UserClaims.verifyIdToken( data, context);
});

exports.getAllUsers = functions.https.onCall((data, context) => {
  return UserClaims.getAllUsers( data, context);
});

// FCM
exports.fcmSend = functions.https.onCall(async(data, context) => {
  return await FCM.sendMessage( data, context );
});

exports.fcmMulticast = functions.https.onCall(async(data, context) => {
  return await FCM.sendMulticast( data, context );
});

// Twilio
exports.sendSMS = functions.https.onCall(async(data, context) => {
  try {
    const message = await Twilio.sendSMS( data, context );
    // console.log( message );
    return message;
  } catch ( error ) {
    return error;
  }
});

// TTS
exports.textTTS = functions.https.onCall(async(data, context) => {
  try {
    TTS.setParams( data.param );
    const audio = await TTS.textTTS( data.text );
    return audio;
  } catch ( e ) {
    throw new functions.https.HttpsError(e.code, e.message);
  }
});
exports.ssmlTTS = functions.https.onCall(async(data, context) => {
  try {
    TTS.setParams( data.param );
    const audio = await TTS.ssmlTTS( data.text );
    return audio;
  } catch ( e ) {
    throw new functions.https.HttpsError(e.code, e.message);
  }
});

// YouTube
exports.youtubeGetInfo = functions.https.onCall(async(data, context) => {
  try {
    const info = await YouTube.getInfo( data, context );
    return info;
  } catch ( e ) {
    throw new functions.https.HttpsError(e.code, e.message);
  }
});

exports.youtubeScraperCaption = functions.https.onCall((data, context) => {
  return YouTube.getScraperCaption( data, context );
});

// Remote Config
exports.getRemoteConfig = functions.https.onCall( async (data, context) => {
  const serviceAccount = defaultConig;
  const projectId = process.env.GCLOUD_PROJECT;

  const accessToken = await RemoteConfig.getAccessToken( serviceAccount );
  return RemoteConfig.getRemoteConfig( projectId, accessToken );
});
exports.getRemoteConfigItem = functions.https.onCall((data, context) => {
  const serviceAccount = defaultConig;
  const projectId = process.env.GCLOUD_PROJECT;
  const item = data.item;

  return RemoteConfig.getRemoteConfigItem( projectId, serviceAccount, item );
});

exports.nlpQuestion = functions.database.ref('/nlp/question')
  .onWrite((change, context) => {
    NLP.question(change, context);
});
