E1:
  -console.warinig: Passive Event Listeners
  -https://stackoverflow.com/questions/50983289/angular-4-added-non-passive-event-listener-to-a-scroll-blocking-mousewheel-e
  +https://www.npmjs.com/package/default-passive-events
  +https://www.npmjs.com/package/ng-event-options
    .NgEventOptionsModule

E2: round-progressbar.component.ts
  -WARNING: sanitizing unsafe style value translateY(-50%) translateX(-50%)--> Performance 늦어짐
// https://stackoverflow.com/questions/50221969/complex-and-multiple-transform-styles-in-angular-2
--> OK: [style.transform]="getOverlayTransform()"
--> ERROR: [style.transform]="'translateY(-50%) translateX(-50%)'" // WARNING: sanitizing unsafe style value translateY(-50%) translateX(-50%)

### Firebase
RUN: $ firebase deploy --only functions:ytdlGetInfo
ERROR: Firebase Cloud Functions : Failed to read credentials from file
    index.js: admin.initializeApp();
SOL: https://stackoverflow.com/questions/54711038/firebase-cloud-functions-failed-to-read-credentials-from-file
--> firebase simulators를 실해하기위해 설정한,GOOGLE_APPLICATION_CREDENTIALS 설정되어 있는 경우 발생
--> deleted the GOOGLE_APPLICATION_CREDENTIALS env variable. Restarted the terminal

### UI
* Attribute property binding for background-image url in Angular 
// -https://stackoverflow.com/questions/37577343/attribute-property-binding-for-background-image-url-in-angular-2 

```css
<div class="profile-image"
  [ngStyle]="{ 'background-image': 'url(' + image + ')'}">
```

### Tensorflow
ERROR: ../node_modules/@tensorflow/tfjs-core/dist/backends/webgl/canvas_util.d.ts:20:61 - error TS2304: Cannot find name 'OffscreenCanvas'.
--> Open: canvas_util.d.ts
--> 추가: /// <reference types="offscreencanvas" />
///: Triple-slash references instruct the compiler to include additional files in the compilation process.
--> @tensorflow/tfjs-core@1.2.6: O.K, 1.2.9: Fail --> /// <reference types="offscreencanvas" /> 삭제됨
